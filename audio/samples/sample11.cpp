#include <fstream>
#include <thread>
#include <boost/asio.hpp>
#include "audio/audio.hpp"
#include <iomanip>

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

// #define COMPILE_ERROR
// #define RUN_ERROR

#if defined(COMPILE_ERROR)
#define SOCKET_SAMPLE_FORMAT sample_format_t::values::signed16_le
#else
#define SOCKET_SAMPLE_FORMAT sample_format_t::values::signed16_be
#endif

#if defined(RUN_ERROR)
#define SOCKET_CHANNELS 1u
#else
#define SOCKET_CHANNELS 2u
#endif


using audio_socket_custom = audio_socket<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, SOCKET_SAMPLE_FORMAT>
>;

using buffer_format = BufferFormat<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, sample_format_t::values::signed16_be>,
        compile_option_t<channels_t , 2>
>;

template<typename FORMAT, typename RAW_BUFFER>
const_typed_buffer<FORMAT> read_buffer(RAW_BUFFER& raw_buffer, std::ifstream& file_stream) {
    file_stream.seekg(0, std::ios_base::end);
    std::streampos file_size = file_stream.tellg();
    raw_buffer.resize(file_size);
    file_stream.seekg(0, std::ios_base::beg);
    file_stream.read(raw_buffer.data(), file_size);
    std::cout << "Read: " << file_size << " bytes" << std::endl;
    return const_typed_buffer<FORMAT>(boost::asio::buffer(raw_buffer));
};

int main(int argc, char *argv[])
{
    if (argc < 2)
        return -1;
    boost::asio::io_service io_service;
    std::vector<char> raw_buffer;
    std::ifstream file_stream(argv[1], std::ios_base::binary);

    auto buffer = read_buffer<buffer_format>(raw_buffer, file_stream);

    audio_socket_custom sink(io_service,
              device_id_t("default"),
              direction_t(direction_t::values::playback),
              resample_t(false),
              sample_rate_t(48_kHz),
              latency_t(200_ms),
              period_t(200_ms / 4),
              min_available_t(2204_frames),
              channels_t(SOCKET_CHANNELS)
    );

    sink.write(buffer);

    sink.close();
}

