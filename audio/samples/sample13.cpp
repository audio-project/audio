#include <fstream>
#include <sstream>
#include <audio/audio.hpp>

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

using i_2ch_s16_le = BufferFormat<
        compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_le>,
        compile_option_t<options::channels_t , 2>
>;

template<typename FORMAT, typename RAW_BUFFER>
const_typed_buffer<FORMAT> read_buffer(RAW_BUFFER& raw_buffer, std::ifstream& file_stream) {
    file_stream.seekg(0, std::ios_base::end);
    std::streampos file_size = file_stream.tellg();
    raw_buffer.resize(file_size);
    file_stream.seekg(0, std::ios_base::beg);
    file_stream.read(raw_buffer.data(), file_size);
    std::cout << "Read: " << file_size << " bytes" << std::endl;
    return const_typed_buffer<FORMAT>(boost::asio::buffer(raw_buffer));
};

class audio_player
{
public:
    audio_player(boost::asio::io_service& io_service)
        : sink_(io_service,
                device_id_t("sysdefault"),
                sample_format_t(sample_format_t::values::signed16_le),
                container_format_t(container_format_t::values::interleaved),
                direction_t(direction_t::values::playback),
                channels_t(2u),
                resample_t(false),
                sample_rate_t(48000_Hz),
                latency_t(200_ms),
                period_t(200_ms / 4)) {}

    void play(std::string filename)
    {
        std::ifstream file(filename);
        std::vector<char> raw_buffer;
        auto buffer = read_buffer<i_2ch_s16_le>(raw_buffer, file);
        sink_.write(buffer);
    }

private:
    // Output device
    audio_socket<> sink_;
};

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " <wav file to play>" << std::endl;
        return -1;
    }
   
		std::cout << std::endl << "Synchronous playback." << std::endl << std::endl;

		boost::asio::io_service io_service;
    audio_player player(io_service);
    
		player.play(argv[1]);
}
