#include <thread>
#include <boost/asio.hpp>
#include <audio/audio.hpp>
#include <stdio.h>

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

using i_2ch_s16_le = BufferFormat<
        compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_le>,
        compile_option_t<options::channels_t , 2>
>;

class audio_player
{
public:
    audio_player(boost::asio::io_service& io_service)
        : source_(io_service),
          sink_(io_service,
                device_id_t("sysdefault"),
                sample_format_t(sample_format_t::values::signed16_le),
                container_format_t(container_format_t::values::interleaved),
                direction_t(direction_t::values::playback),
                channels_t(2u),
                resample_t(false),
                sample_rate_t(48000_Hz),
                latency_t(200_ms),
                period_t(200_ms / 4)),
          buffer_(10240) {}

    void play_async(std::string filename)
    {
        int descriptor = open(filename.c_str(), O_RDONLY);
        source_.assign(descriptor);
        read_from_file_async();
    }

protected:
    void read_from_file_async()
    {
        boost::asio::async_read(source_, buffer_, 
            [this](boost::system::error_code ec, size_t)
            { 
                if (ec)
                    return;

                play_buffer_async();
            }
        );
    }

    void play_buffer_async() 
    {
        sink_.async_write(const_typed_buffers_1<i_2ch_s16_le>(buffer_.data()), 
            [this](boost::system::error_code ec, size_t size)
            {   
                if (size == 0) return;
                if (ec) return;
                
                buffer_.consume(size);
                
                if (buffer_.size() > 0)
                {
                    play_buffer_async();
                    return;
                }

                read_from_file_async();
            });
    }

private:
    // Input file
    boost::asio::posix::stream_descriptor source_;

    // Output device
    audio_socket<> sink_;

    // Stream buffer
    boost::asio::streambuf buffer_;
};

int main(int argc, char** argv)
{
		if (argc < 2)
    {
        std::cout << "Usage: sample8 <wav file to play>" << std::endl; 
        return -1;
    }

		std::cout << std::endl << "Asynchronous playback using two threads." << std::endl << std::endl;
 
 		boost::asio::io_service io_service;
    audio_player player(io_service);

    std::cout << "Playing: " << argv[1] << std::endl;

    player.play_async(argv[1]);
		std::thread run_thread([&]{ io_service.run(); });

    for (auto i = 0; i < 8; i++)
    {
        std::cout << i << std::endl;
        sleep(1);
    }
		
		run_thread.join();

    return 0;
}
