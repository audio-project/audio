#include <vector>
#include <boost/asio.hpp>
#include <thread>
#include <fstream>

#include "audio/audio.hpp"
#include <iostream>

std::vector<char> v(12);
boost::asio::io_service io_service;
trial::audio::audio_socket source(io_service, "default");
trial::audio::audio_socket sink(io_service, "default");

void kasperHandler(boost::system::error_code ec, std::size_t s)
{
	std::cout << "Kristian is doing stuff! " << s << std::endl;
}

void kristianHandler(const boost::system::error_code& ec, std::size_t st)
{
	for (auto c : v)
		std::cout << c;
	
	source.async_read(boost::asio::buffer(v.data(), 12), kristianHandler);
	sink.async_write(boost::asio::buffer(v), kasperHandler);
}



int main() 
{

	std::string hello = "Hello\n";

	source.async_read(boost::asio::buffer(v.data(), 12), kristianHandler);

	std::thread run_thread([&]{io_service.run();});

	while(1)
	{
		sleep(4);
		std::cout << "I am still here" << std::endl;
	}
	
	return 0;
}
