#include <fstream>
#include <string>
#include <thread>
#include <boost/asio.hpp>
#include "audio/audio.hpp"
#include <alsa/pcm.h>
#include <boost/function.hpp>
#include <iostream>
#include <iomanip>
#include <audio/util.hpp>

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

#define BSIZE 60000

boost::asio::io_service io_service;

using i_2ch_s16_le = BufferFormat<
        compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_le>,
        compile_option_t<options::channels_t , 2>
>;


audio_socket<> sink(io_service,
                  device_id_t("default"),
                  sample_format_t(sample_format_t::values::signed16_le),
                  container_format_t(container_format_t::values::interleaved),
                  direction_t(direction_t::values::playback),
                  channels_t(2),
                  resample_t(false),
                  sample_rate_t(44100_Hz),
                  latency_t(200_ms),
                  period_t(200_ms / 4)
);

std::ifstream binaryIo;
std::array<char, 18667084> buf;
boost::asio::streambuf buffer(18667084);
boost::asio::streambuf buffer2(BSIZE);

  
void kasperHandler(boost::system::error_code ec, std::size_t s)
{

    buffer2.consume(s);
    auto sa = boost::asio::buffer_copy(buffer2.prepare(s), buffer.data(), s);
    buffer.consume(sa);
    buffer2.commit(sa);


    if (s > 0) {
        std::cout << "Consumed: " << s << " bytes [";
        std::cout << std::setfill('0') << std::setw(3) << 100-((buffer.size() * 100 ) / buffer.max_size());
        std::cout << "%] - ";
        std::cout << std::setfill(' ') << std::setw(8) << buffer.size();
        std::cout << " remaining" << std::endl;
    }

    if (buffer.size() > 0){
		sink.async_write(const_typed_buffers_1<i_2ch_s16_le>(buffer2.data()), kasperHandler);
	} else {
        std::cout << " - No more in the buffer\n";
    }

}




int main(int argc, char *argv[]) 
{
	std::cout << argv[0] << std::endl;
	std::cout << argv[1] << std::endl;
    
    
    binaryIo.open(argv[1], std::ios_base::binary);
    binaryIo.read(buf.data(), 18667084);
       
    boost::asio::buffer_copy(buffer.prepare(18667084), boost::asio::buffer(buf));
    buffer.commit(18667084);

    auto sa = boost::asio::buffer_copy(buffer2.prepare(BSIZE), buffer.data(), BSIZE);
    buffer.consume(sa);
    buffer2.commit(sa);

    auto native_device = sink.native_device();
    auto native_handle = sink.native_handle();
	

	sink.async_write(const_typed_buffers_1<i_2ch_s16_le>(buffer2.data()), kasperHandler);
   

	std::thread run_thread([&]{io_service.run();});
   
 
    run_thread.join();

  sink.close();
}

