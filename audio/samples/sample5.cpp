#include <fstream>
#include <string>
#include <thread>
#include <boost/asio.hpp>
#include "audio/audio.hpp"
#include <alsa/pcm.h>
#include <boost/function.hpp>
#include <iostream>

#define BUFFER_SIZE 1000000

enum class BufferStatus {
	Idle,
	Prebuffering,
	Somebuffered,
	Allbuffered
};

struct Player; 

struct Loader {
	
	Player* player_;
	boost::asio::posix::stream_descriptor in_;
	int file_descriptor_;

	Loader(Player* player, boost::asio::io_service& io_service) : player_(player), in_(io_service), file_descriptor_(0) {}

	std::function<void (boost::system::error_code ec, std::size_t s)> handler();
	
	std::function<void (boost::system::error_code ec, std::size_t s)> pre_handler();

	void assign(const char* filename);

	void async_read(std::size_t s);
	
	void async_pre_read(std::size_t s);

};

struct Speaker {

	Player* player_;
	trial::audio::audio_socket sink_;

	Speaker(Player* player, boost::asio::io_service& io_service, int freq) : player_(player), sink_(io_service, "default") 
	{
  		snd_pcm_t* native_handle = sink_.native_handle();
  		snd_pcm_set_params(native_handle, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, 2, freq, 0, 200000);
	}

	~Speaker() {
		sink_.close();
	}

	std::function<void (boost::system::error_code, std::size_t)> handler();

	void async_write();


};

struct Player {

	Speaker speaker_;
	Loader loader_;
	bool running_;
	std::unique_ptr<boost::asio::io_service::work> work_;
	boost::asio::streambuf buffer_;
	BufferStatus buffer_status;
	
	Player(boost::asio::io_service& io_service) : speaker_(this, io_service, 44100), loader_(this, io_service), running_(false), work_(new boost::asio::io_service::work(io_service)), buffer_(BUFFER_SIZE), buffer_status(BufferStatus::Idle) {}

	void async_read() {
		if ((buffer_.size() < (buffer_.max_size() / 3)) && (running_)) {
			std::cout << "Getting more - live\n";
			loader_.async_read(25000);
		}
	}
	
	void buffer_preloaded() {
		speaker_.async_write();
	}

	void reset() {
		work_.reset();
	}

	void async_write_more() {
		speaker_.async_write();
	}
	
	void async_pre_read(std::size_t s) {
		loader_.async_pre_read(s);
	}

	void async_play(const char* filename) {
		std::cout << "Prebuffering: " << filename << std::endl;
		loader_.assign(filename);
		//async_pre_read(buffer_.max_size() / 10);
		async_pre_read(buffer_.max_size());
	}
	
	void print_buffer_status() {
		std::cout << "Buffer [" << (buffer_.size() * 100) / buffer_.max_size() << "%] [" << buffer_.size()  <<  "]\n";
	}
	

};

std::function<void (boost::system::error_code, std::size_t)> Loader::pre_handler() {
		Player* player = player_;
		return [player](boost::system::error_code ec, std::size_t s) {
			
					if (ec != 0) {
							std::cout << "Preload error\n";
							player->running_ = false;
							player->buffer_preloaded();
					} else {
							std::cout << "Pre buffered\n";
							player->buffer_.commit(s);
							player->print_buffer_status();
							if (player->buffer_.size() < (player->buffer_.max_size() / 2)) {
								player->async_pre_read(player->buffer_.max_size() / 10);
							} else {
								player->running_ = true;
								player->buffer_preloaded();
							}
							
							
					}
					
	        	
		};
	}

std::function<void (boost::system::error_code, std::size_t)> Loader::handler() {
		Player* player = player_;
		return [player](boost::system::error_code ec, std::size_t s) {
			
					if (ec != 0) {
							std::cout << "No more data\n";
							player->running_ = false;
					} else {
							std::cout << "Committing\n";
							player->buffer_.commit(s);
					}
	        	
		};
	}
	
void Loader::assign(const char* filename) {
	file_descriptor_ = open(filename, O_RDONLY);
	in_.assign(file_descriptor_);
}

void Loader::async_read(std::size_t s ) {
	boost::asio::async_read(in_, player_->buffer_.prepare(s), handler());
}

void Loader::async_pre_read(std::size_t s ) {
	boost::asio::async_read(in_, player_->buffer_.prepare(s), pre_handler());
}



std::function<void (boost::system::error_code, std::size_t)> Speaker::handler() {
	Player* player = player_;

    return [player](boost::system::error_code ec, std::size_t s) {
		
		player->buffer_.consume(s*4);
		player->print_buffer_status();
		
		if (player->buffer_.size() > 0){
			player->async_write_more();
			player->async_read();
       	} else {
			if (player->running_) {
				std::cout << "Buffer underrun, stopping work\n";
				player->reset();
			} else {
				std::cout << "Song ended, stopping work\n";
				player->reset();
			}
		}

	};
}

void Speaker::async_write() {
   	sink_.async_write(boost::asio::buffer(player_->buffer_.data()), handler());
}

	
	
int main(int argc, char *argv[])
{
    std::cout << argv[0] << std::endl;
    std::cout << argv[1] << std::endl;

	boost::asio::io_service io_service;

	Player player(io_service);
	

	std::thread run_thread([&]{io_service.run();});
	
	player.async_play(argv[1]);
  
  	std::cout << "DONT SEGFAULT ME BRO" << std::endl;
	run_thread.join();
}
