#include <iostream>
#include <fstream>
#include <thread>
#include <boost/asio.hpp>
#include "audio/audio.hpp"

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;


using i_2ch_s32_le = BufferFormat<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, sample_format_t::values::signed32_le>,
        compile_option_t<channels_t , 2>
>;

using i_2ch_s16_be = BufferFormat<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, sample_format_t::values::signed16_be>,
        compile_option_t<channels_t , 2>
>;


template<typename FORMAT, typename RAW_BUFFER>
const_typed_buffer<FORMAT> read_buffer(RAW_BUFFER& raw_buffer, std::ifstream& file_stream) {
    file_stream.seekg(0, std::ios_base::end);
    std::streampos file_size = file_stream.tellg();
    raw_buffer.resize(file_size);
    file_stream.seekg(0, std::ios_base::beg);
    file_stream.read(raw_buffer.data(), file_size);
    std::cout << "Read: " << file_size << " bytes" << std::endl;
    return const_typed_buffer<FORMAT>(boost::asio::buffer(raw_buffer));
};


template<typename Buffer, typename BufferType = typename Buffer::pcm_format>
void play(const Buffer& buffer) {
    boost::asio::io_service io_service;
    error_code ec;
    using asocket =
            audio_socket<
                    typename BufferType::container,
                    typename BufferType::sample,
                    typename BufferType::channels
             >;

    asocket sink(io_service);

    sink.open(ec,
              device_id_t("default"),
              direction_t(direction_t::values::playback),
              resample_t(false),
              sample_rate_t(48_kHz),
              latency_t(200_ms),
              period_t(200_ms / 4),
              min_available_t(2204_frames)
    );

    sink.write(buffer);

    sink.close();
}


int main(int argc, char *argv[])
{
    if (argc < 3)
        return -1;

		std::cout << std::endl << "Use of type-safe buffers: different formats." << std::endl << std::endl;
		std::vector<char> buffer;
    std::ifstream file_stream(argv[1], std::ios_base::binary);

    if (argv[2][0] == 'l') {
        play(read_buffer<i_2ch_s32_le>(buffer, file_stream));
    } else if(argv[2][0] == 'b') {
       play(read_buffer<i_2ch_s16_be>(buffer, file_stream));
    }

    return 0;

}

