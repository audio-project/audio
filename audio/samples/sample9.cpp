#include <cstdlib>
#include <iostream>
#include <boost/asio.hpp>
#include <audio/audio.hpp>


using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

using i_2ch_s16_le = BufferFormat<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, sample_format_t::values::signed16_le>,
        compile_option_t<channels_t , 2>
>;

class audio_player
{
public:
    audio_player(boost::asio::io_service& io_service, size_t ttr)
        : sink_(io_service,
                device_id_t("default"),
                direction_t(direction_t::values::playback),
                resample_t(false),
                sample_rate_t(44100_Hz),
                latency_t(200_ms),
                period_t(200_ms / 4)),
          source_(io_service,
                  device_id_t("default"),
                  direction_t(direction_t::values::capture),
                  resample_t(false),
                  sample_rate_t(44100_Hz),
                  latency_t(200_ms),
                  period_t(200_ms / 4)),
          buffer_(44100 * 4 * ttr), 
          timeToRecord_(ttr)
    {
    }

    void capture()
    {
        read_async();
    }

protected:
    void read_async()
    {
            auto free_space = buffer_.max_size() - buffer_.size();
            auto read_size = (free_space < 1500 * 4 ? free_space : 1500 * 4);

            source_.async_read(
                    mutable_typed_buffers_1<i_2ch_s16_le>(buffer_.prepare(read_size)),
                [this](boost::system::error_code ec, size_t size)
                {
                    std::cout << "Read Error code: " << ec << " Read size: " << size << std::endl;
                    if (ec)
                        return;

                    buffer_.commit(size);

                    if(buffer_.size() < (44100 * timeToRecord_ * 4))
                    {
                        read_async();
                        return;
                    }

                    play_buffer_async();
                }
            );
    }

    void play_buffer_async()
    {
        sink_.async_write(const_typed_buffers_1<i_2ch_s16_le>(buffer_.data()),
            [this](boost::system::error_code ec, size_t size)
            {             
                std::cout << "Play Error Code: " << ec << " Play size: " << size << std::endl;
                if (ec)
                    return;
                
                buffer_.consume(size);
                
                if (size == 0)
                    return;

                play_buffer_async();

            });
    }

private:
    // Output device
    trial::audio::audio_socket<
        compile_option_t<container_format_t, container_format_t::values::interleaved>,
        compile_option_t<sample_format_t, sample_format_t::values::signed16_le>,
        compile_option_t<channels_t , 2>    
    > sink_;
    
		// Input device
    trial::audio::audio_socket<
            compile_option_t<container_format_t, container_format_t::values::interleaved>,
            compile_option_t<sample_format_t, sample_format_t::values::signed16_le>,
            compile_option_t<channels_t , 2>
    > source_;

    // Stream buffer
    boost::asio::streambuf buffer_;
    
    size_t timeToRecord_;
};

int main(int argc, char** argv)
{
    if(argc < 2){
        std::cout << "Usage: sample9 <time to record i.e.: 10>" << std::endl;
        return -1;
    }
		
	  std::cout << std::endl << "Asynchronous recording and playback." << std::endl << std::endl;	

    boost::asio::io_service io_service;
    audio_player player(io_service, strtoul(argv[1], NULL, 0));

    player.capture();

    io_service.run();

    return 0;
}
