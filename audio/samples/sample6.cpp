#include <fstream>
#include <string>
#include <thread>
#include <boost/asio.hpp>
#include "audio/audio.hpp"
#include <alsa/pcm.h>
#include <boost/function.hpp>
#include <iostream>

#define BUFFER_SIZE 100000
#define LOADER_BUFFER_SIZE 18667086

enum class BufferStatus {
	Idle,
	Prebuffering,
	Somebuffered,
	Allbuffered
};

struct Player; 

struct Loader {
	
	Player* player_;
	boost::asio::posix::stream_descriptor in_;
	int file_descriptor_;
	

	Loader(Player* player, boost::asio::io_service& io_service) : player_(player), in_(io_service), file_descriptor_(0), buffer_(LOADER_BUFFER_SIZE) {}

	void assign(const char* filename);
	
	boost::asio::streambuf* buffer() { return &buffer_; };
	
	private:
		boost::asio::streambuf buffer_;

};

struct Speaker {

	Player* player_;
	trial::audio::audio_socket sink_;

	Speaker(Player* player, boost::asio::io_service& io_service, int freq) : player_(player), sink_(io_service, "default") 
	{
  		snd_pcm_t* native_handle = sink_.native_handle();
  		snd_pcm_set_params(native_handle, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, 2, freq, 0, 200000);
	}

	~Speaker() {
		sink_.close();
	}

	std::function<void (boost::system::error_code, std::size_t)> handler();

	void async_write();


};

struct Player {

	Speaker speaker_;
	Loader loader_;
	bool running_;
	std::unique_ptr<boost::asio::io_service::work> work_;
	boost::asio::streambuf buffer_;
	BufferStatus buffer_status;
	
	Player(boost::asio::io_service& io_service) : speaker_(this, io_service, 44100), loader_(this, io_service), running_(false), work_(new boost::asio::io_service::work(io_service)), buffer_(BUFFER_SIZE), buffer_status(BufferStatus::Idle) {}

	void print_buffer_status() {
		std::cout << "Buffer [" << (buffer_.size() * 100) / buffer_.max_size() << "%] [" << buffer_.size()  <<  "]\n";
	}
	
	void read() {
		if ((buffer_.size() < (buffer_.max_size() / 3)) && (buffered_data())) {
			std::cout << "Getting more\n";
			
			std::size_t bytes_copied = buffer_copy(
				buffer_.prepare(buffer_.max_size() - buffer_.size()), // target's output sequence
				loader_.buffer()->data()
			);                // source's input sequence
			
			// Explicitly move target's output sequence to its input sequence.
			buffer_.commit(bytes_copied);
			loader_.buffer()->consume(bytes_copied);
			print_buffer_status();
		}
	}

	void reset() {
		work_.reset();
	}

	void async_write() {
		read();
		read();
		speaker_.async_write();
	}
	
	bool buffered_data() {
			return loader_.buffer()->size() > 0;
	}
	

	void async_play(const char* filename) {
		std::cout << "Prebuffering: " << filename << std::endl;
		loader_.assign(filename);
		async_write();
	}
	
	
	

};
	
	
	
void Loader::assign(const char* filename) {
	file_descriptor_ = open(filename, O_RDONLY);
	in_.assign(file_descriptor_);
	boost::asio::read(in_, buffer_);
}



std::function<void (boost::system::error_code, std::size_t)> Speaker::handler() {
	Player* player = player_;

    return [player](boost::system::error_code ec, std::size_t s) {
		
		player->buffer_.consume(s*4);
		player->print_buffer_status();
		
		if ((player->buffer_.size() > 0) || (player->buffered_data())){
			player->async_write();
       	} else {
			std::cout << "Song ended, stopping work\n";
			player->reset();
		}

	};
}

void Speaker::async_write() {
   	sink_.async_write(boost::asio::buffer(player_->buffer_.data()), handler());
}

	
	
int main(int argc, char *argv[])
{
    std::cout << argv[0] << std::endl;
    std::cout << argv[1] << std::endl;

	boost::asio::io_service io_service;

	Player player(io_service);
	

	std::thread run_thread([&]{io_service.run();});
	
	player.async_play(argv[1]);
  
	run_thread.join();
}
