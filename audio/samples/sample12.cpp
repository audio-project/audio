#include <fstream>
#include <thread>
#include <iostream>
#include <boost/asio.hpp>
#include <audio/audio.hpp>

using namespace trial::audio;
using namespace trial::audio::units;
using namespace trial::audio::options;
using namespace trial::audio::util;

using i_2ch_s16_le = BufferFormat<
        compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_le>,
        compile_option_t<options::channels_t , 2>
>;

using audio_socket_custom_le = audio_socket<
                        i_2ch_s16_le::sample,
                        i_2ch_s16_le::container
>;

template<typename FORMAT, typename RAW_BUFFER>
const_typed_buffers_1<FORMAT> read_buffer(RAW_BUFFER& raw_buffer, std::ifstream& file_stream) {
    file_stream.seekg(0, std::ios_base::end);
    std::streampos file_size = file_stream.tellg();
    raw_buffer.resize(file_size);
    file_stream.seekg(0, std::ios_base::beg);
    file_stream.read(raw_buffer.data(), file_size);
    std::cout << "Read: " << file_size << " bytes" << std::endl;
    return const_typed_buffers_1<FORMAT>(boost::asio::buffer(raw_buffer));
};


int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cout << "Usage: sample12 <path to raw file>" << std::endl;
        return -1;
    }

		std::cout << std::endl << "Use of free function async_write from Asio." << std::endl << std::endl;

		boost::asio::io_service io_service;
    audio_socket_custom_le sink_le(io_service,
                               device_id_t("default"),
                               direction_t(direction_t::values::playback),
                               channels_t(2u),
                               resample_t(false),
                               sample_rate_t(44100_Hz),
                               latency_t(200_ms),
                               period_t(200_ms / 4)
    );
    
		std::vector<char> raw_buffer;
    std::ifstream file_stream(argv[1], std::ios_base::binary);
	  auto buffer =	read_buffer<i_2ch_s16_le>(raw_buffer, file_stream);
    
    boost::asio::async_write(sink_le, buffer, [](boost::system::error_code, std::size_t){});

    std::thread run_thread([&]{io_service.run();});

    for (auto i = 0; i < 8; i++)
    {
        std::cout << i << std::endl;
        sleep(1);
    }

    run_thread.join();

    sink_le.close();
    
    return 0;
}

