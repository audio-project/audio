#ifndef TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_IPP
#define TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_IPP

#include <boost/asio/detail/config.hpp>

#include <audio/error_codes.hpp>
#include <audio/audio_socket_base.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {


} // namespace audio 
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_IPP