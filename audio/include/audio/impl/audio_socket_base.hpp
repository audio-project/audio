#ifndef TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_HPP
#define TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {


} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_IMPL_AUDIO_SOCKET_BASE_HPP