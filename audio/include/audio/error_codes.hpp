#ifndef TRIAL_AUDIO_ERROR_CODES_HPP
#define TRIAL_AUDIO_ERROR_CODES_HPP

#include <boost/asio/detail/config.hpp>
#include <boost/asio/error.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
    
    using error_code = boost::system::error_code;

} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_ERROR_CODES_HPP