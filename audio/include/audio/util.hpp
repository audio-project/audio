#ifndef TRIAL_AUDIO_UTIL_HPP
#define TRIAL_AUDIO_UTIL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/config.hpp>

#include <audio/error_codes.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace util {

template<typename ReturnType>
inline ReturnType error_wrapper(ReturnType return_value,
                                audio::error_code &ec) {

    if (errno == ENOTSOCK)
        errno = 0;

    ec = audio::error_code(errno, audio::error::get_system_category());

    return return_value;
}

template<typename ...Args>
struct List;

template <typename...>
struct is_one_of {
    static constexpr bool value = false;
};

template <typename F, typename S, typename... T>
struct is_one_of<F, S, T...> {
    static constexpr bool value =
            std::is_same<F, S>::value || is_one_of<F, T...>::value;
};

template<typename T, typename U>
struct is_in_list;

template <typename T, template <typename...> class L, typename ...Args>
struct is_in_list<T, L<Args...>> {
    static constexpr bool value = is_one_of<T, Args...>::value;
};

template <typename...>
struct has_one_of {
    static constexpr bool value = false;
};

template <typename F, typename S, typename... T>
struct has_one_of<F, S, T...> {
    static constexpr bool value =
            std::is_same<F, typename S::value_type>::value || has_one_of<F, T...>::value;
};


template <typename...>
struct has_duplicate {
    static constexpr bool value = false;
};

template <typename F, typename... T>
struct has_duplicate<F, T...> {
    static constexpr bool value = is_one_of<F, T...>::value || has_duplicate<T...>::value;
};

template <typename...>
struct all_in_list {
    static constexpr bool value = true;
};

template <typename V, typename... F, typename T>
struct all_in_list<List<V, F...>, T> {
    static constexpr bool value = is_in_list<V, T>::value && all_in_list<List<F...>, T>::value;
};


// Extract the CompileOption that handles the Option
template<typename T, typename ...>
struct get_compiled_setting;

template<typename T, typename V, typename ...CompiledSettings>
struct get_compiled_setting<T, V, CompiledSettings...> {
    using type = typename get_compiled_setting<T, CompiledSettings...>::type;
};

template<typename T, typename ...CompiledSettings>
struct get_compiled_setting<typename T::option_type, T, CompiledSettings...> {
    using type = T;
};


template<typename T, typename ...Settings>
void get_runtime_setting_impl(T& value, T setting, Settings...) {
    value = setting;
}

template<typename T, typename U, typename ...Settings>
void get_runtime_setting_impl(T& value, U, Settings... settings) {
    get_runtime_setting_impl(value, settings...);
}

template<typename T, typename ...Settings>
T get_runtime_setting(Settings... settings) {
    static_assert(is_one_of<T, Settings...>::value, "Missing Required Setting");
    T value;
    get_runtime_setting_impl(value, settings...);
    return value;
}


template<typename Format>
struct const_typed_buffers_1 : public boost::asio::const_buffers_1
{
    typedef Format pcm_format;
    using boost::asio::const_buffers_1::const_buffers_1;

    //template<typename T>
    //const_buffer(const T& buffer) : const_buffers_1(static_cast<const void*>(buffer.data()), buffer.size()) {};
};

template<typename Format>
struct const_typed_buffer : public boost::asio::const_buffer
{
    typedef Format pcm_format;
    using boost::asio::const_buffer::const_buffer;

    //template<typename T>
    //const_buffer(const T& buffer) : const_buffers_1(static_cast<const void*>(buffer.data()), buffer.size()) {};
};

template<typename Format>
struct mutable_typed_buffers_1 : public boost::asio::mutable_buffers_1
{
    typedef Format pcm_format;
    using boost::asio::mutable_buffers_1::mutable_buffers_1;
};

template<typename Format>
struct mutable_typed_buffer : public boost::asio::mutable_buffer
{
    typedef Format pcm_format;
    using boost::asio::mutable_buffer::mutable_buffer;
};

template<typename T>
struct always_void {
    typedef void type;
};



}
}
}

#endif //TRIAL_AUDIO_UTIL_HPP