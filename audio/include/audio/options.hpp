#ifndef TRIAL_AUDIO_OPTIONS_HPP
#define TRIAL_AUDIO_OPTIONS_HPP

#include <boost/asio/detail/config.hpp>
#include <audio/units.hpp>
#include <boost/variant.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {

/*! \namespace options
    \brief A test class.

    A more generatorsed class description.
*/
namespace options {
namespace generators {

//region Option Generator

template<typename ValueType>
struct option_class_t {
    using value_type = ValueType;
    using integral_type = typename ValueType::value_type;

    option_class_t() = default;

    option_class_t(const option_class_t &) = default;

    option_class_t &operator=(const option_class_t &) = default;

    bool operator==(const value_type &other) {
        return value_ == other.value_;
    }

    explicit option_class_t(value_type value) : value_(value) {}

    const value_type value() const {
        return value_;
    }

private:
    value_type value_;
};

template<typename Integral>
struct option_integral_t {
    using value_type = Integral;
    using integral_type = Integral;

    option_integral_t() = default;

    option_integral_t(const option_integral_t &) = default;

    option_integral_t &operator=(const option_integral_t &) = default;

    bool operator==(const value_type &other) {
        return value_ == other.value_;
    }

    explicit option_integral_t(value_type value) : value_(value) {}

    const value_type value() const {
        return value_;
    }

private:
    value_type value_;
};

template<typename Enum>
struct option_enum_t {
    using value_type = Enum;
    using integral_type = Enum;

    using values = Enum;

    option_enum_t() = default;

    option_enum_t(const option_enum_t &) = default;

    option_enum_t &operator=(const option_enum_t &) = default;

    bool operator==(const value_type &other) {
        return value_ == other.value_;
    }

    explicit option_enum_t(value_type value) : value_(value) {}

    const value_type value() const {
        return value_;
    }

private:
    value_type value_;
};

//endregion
}

//region Options

struct resample_t : generators::option_integral_t<bool> {
    using generators::option_integral_t<bool>::option_integral_t;
};

enum class direction_enum_t { playback, capture };
struct direction_t : generators::option_enum_t<direction_enum_t> {
    using generators::option_enum_t<direction_enum_t>::option_enum_t;
};

enum class container_format_enum_t { interleaved, planar };
struct container_format_t : generators::option_enum_t<container_format_enum_t> {
    using generators::option_enum_t<container_format_enum_t>::option_enum_t;
};

enum sample_format_enum_t {
    signed8 = 0,
    unsigned8,
    signed16_le,
    signed16_be,
    unsigned16_le,
    unsigned16_be,
    signed24_le,
    signed24_be,
    unsigned24_le,
    unsigned24_be,
    signed32_le,
    signed32_be,
    unsigned32_le,
    unsigned32_be,
    float_le,
    float_be,
    float64_le,
    float64_be,
};
struct sample_format_t : generators::option_enum_t<sample_format_enum_t> {
    using generators::option_enum_t<sample_format_enum_t>::option_enum_t;
};

struct start_threshold_t : generators::option_class_t<units::Frame> {
    using generators::option_class_t<units::Frame>::option_class_t;
};

struct min_available_t : generators::option_class_t<units::Frame> {
    using generators::option_class_t<units::Frame>::option_class_t;
};

struct sample_rate_t : generators::option_class_t<units::Hertz> {
    using generators::option_class_t<units::Hertz>::option_class_t;
};

struct channels_t : generators::option_integral_t<std::size_t> {
    using generators::option_integral_t<std::size_t>::option_integral_t;
};


struct period_t {
public:

    typedef boost::variant<units::Frame, units::Microsecond> value_type;

    explicit period_t(const units::Frame &frames) : value_(frames) {}

    explicit period_t(const units::Microsecond &seconds) : value_(seconds) {}

    period_t() = default;
    period_t(const period_t&) = default;
    period_t& operator=(const period_t&) = default;

    const value_type value() const  {
        return value_;
    }

private:
    value_type value_;
};

struct latency_t {
public:

    typedef boost::variant<units::Frame, units::Microsecond> value_type;

    explicit latency_t(const units::Frame &frames) : value_(frames) {}

    explicit latency_t(const units::Microsecond &seconds) : value_(seconds) {}

    latency_t() = default;
    latency_t(const latency_t&) = default;
    latency_t& operator=(const latency_t&) = default;

    const value_type& value() const {
        return value_;
    }

private:
    value_type value_;
};

struct device_id_t {
    typedef std::string value_type;

    device_id_t() : device_id_t("default") {};

    explicit device_id_t(value_type name) : value_(name) {};

    device_id_t(const device_id_t&) = default;
    device_id_t& operator=(const device_id_t&) = default;

    const value_type& value() const {
        return value_;
    }

private:
    value_type value_;

};

//endregion

//region Compiletime Option Generator

template<typename Option, typename Option::integral_type Value>
struct compile_option_t {
    using option_type   = Option;
    using value_type    = typename Option::value_type;
    using integral_type = typename Option::integral_type;

    static constexpr integral_type integral_value = Value;

    static constexpr option_type value() {
        return option_type{value_type{integral_type{integral_value}}};
    }
};

//endregion


template<sample_format_t::values value>
struct sample_size;

template<>
struct sample_size<sample_format_t::values::signed16_le> {
    static const std::size_t value = 2;
};

template<>
struct sample_size<sample_format_t::values::signed16_be> {
    static const std::size_t value = 2;
};

template<>
struct sample_size<sample_format_t::values::signed32_le> {
    static const std::size_t value = 4;
};

template<>
struct sample_size<sample_format_t::values::signed32_be> {
    static const std::size_t value = 4;
};

template<typename PCMFormat>
struct frame_size {
    static const std::size_t value = PCMFormat::channels::integral_value * sample_size<PCMFormat::sample::integral_value>::value;
};


} // namespace options


template<typename Container, typename Sample, typename Channels>
struct BufferFormat {
    using container = Container;
    using sample = Sample;
    using channels = Channels;
};
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_OPTIONS_HPP