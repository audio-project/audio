#ifndef TRIAL_AUDIO_ERROR_HPP
#define TRIAL_AUDIO_ERROR_HPP

#include <boost/asio/detail/config.hpp>
#include <boost/asio/error.hpp>

#include "audio/error_codes.hpp"

#include <boost/asio/detail/push_options.hpp>

#define CHANNELS_INVALID 1

namespace trial {
namespace audio {
namespace error {

    using namespace boost::asio::error;

    enum options_errors
    {
        /// Host not found (authoritative).
                channels_invalid = CHANNELS_INVALID,


    };


}
} // namespace audio
} // namespace trial

namespace boost {
namespace system {
template<> struct is_error_code_enum<trial::audio::error::options_errors>
{
    static const bool value = true;
};
}
}

namespace trial {
namespace audio {
namespace error {


namespace detail {
class options_category : public boost::system::error_category
{
public:
    const char* name() const BOOST_ASIO_ERROR_CATEGORY_NOEXCEPT
    {
        return "audio.options";
    }

    std::string message(int value) const
    {
        if (value == error::channels_invalid)
            return "Channels number invalid";
        return "audio.options error";
    }
};
}

const boost::system::error_category& get_options_category()
{
    static detail::options_category instance;
    return instance;
}

inline boost::system::error_code make_error_code(options_errors e)
{
    return boost::system::error_code(
            static_cast<int>(e), get_options_category());
}





}
} // namespace audio
} // namespace trial






#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_ERROR_HPP