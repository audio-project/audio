#ifndef TRIAL_AUDIO_AUDIO_SOCKET_SERVICE_HPP
#define TRIAL_AUDIO_AUDIO_SOCKET_SERVICE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/config.hpp>

#include <boost/asio/async_result.hpp>
#include <boost/asio/io_service.hpp>

#include "audio/error_codes.hpp"
#include "audio/detail/audio_socket_service.hpp"
#include "audio/detail/audio_device_service.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {


template<typename AudioDeviceService>
class basic_audio_socket_service_t
    : public boost::asio::detail::service_base<basic_audio_socket_service_t<AudioDeviceService>>
{
public:

    typedef detail::audio_socket_service<AudioDeviceService> service_impl_type;

    typedef typename service_impl_type::implementation_type implementation_type;

    typedef typename service_impl_type::device_t device_t;

    typedef typename service_impl_type::handle_t handle_t;
  
  
    /// Construct a new audio socket service for the specified io_service.
    explicit basic_audio_socket_service_t(boost::asio::io_service& io_service)
      : boost::asio::detail::service_base<basic_audio_socket_service_t>(io_service),
          service_impl_(io_service)
    {
    }


    // Basic IO Service Requirements

    /// Construct an audio socket implementation.
    void construct(implementation_type& impl)
    {
        service_impl_.construct(impl);
    }
    
    /// Destroy an audio socket implementation.
    void destroy(implementation_type& impl)
    {
        service_impl_.destroy(impl);
    }



    /// Open an audio socket implementation.
    template<typename ...Args>
    device_t create_device(audio::error_code& ec, Args... args)
    {
        return service_impl_.create_device(ec, args...);
    }


    
    /// Open an audio socket implementation.
    template<typename ...Args>
    audio::error_code open(implementation_type& impl, 
        audio::error_code& ec, Args... args)
    {
        return service_impl_.open(impl, ec, args...);
    }

    /// Assign an existing native handle to an audio socket.
    audio::error_code assign(implementation_type& impl,
        const device_t& device, audio::error_code& ec)
    {
        return service_impl_.assign(impl, device, ec);
    }

    /// Determine whether the handle is open.
    bool is_open(const implementation_type &impl) const {
        return service_impl_.is_open(impl);
    }
    
    /// Close an audio socket implementation.
    audio::error_code close(implementation_type& impl,
        audio::error_code& ec)
    {
        return service_impl_.close(impl, ec);
    }
    
    /// Get the native handle implementation.
    device_t native_device(const implementation_type& impl) const
    {
        return service_impl_.native_device(impl);
    }

    /// Get the native handle implementation.
    handle_t native_handle(const implementation_type& impl) const
    {
        return service_impl_.native_handle(impl);
    }
    
    
    /// Cancel all asynchronous operations associated with the handle.
    audio::error_code cancel(implementation_type& impl,
        audio::error_code& ec)
    {
        return service_impl_.cancel(impl, ec);
    }

    template <typename... RuntimeSettings>
    audio::error_code set_options(implementation_type &impl, audio::error_code &ec, RuntimeSettings... runtime_settings) {
        return service_impl_.set_options(impl, ec, runtime_settings...);
    }


    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write(implementation_type& impl, 
        const ConstBufferSequence& buffers, audio::error_code& ec)
    {
        return service_impl_.write(impl, buffers, ec);
    }

    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write_some(implementation_type& impl, 
        const ConstBufferSequence& buffers, audio::error_code& ec)
    {
        return service_impl_.write_some(impl, buffers, ec);
    }

    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read(implementation_type& impl, 
        const MutableBufferSequence& buffers, audio::error_code& ec)
    {
        return service_impl_.read(impl, buffers, ec);
    }
    
    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read_some(implementation_type& impl, 
        const MutableBufferSequence& buffers, audio::error_code& ec)
    {
        return service_impl_.read_some(impl, buffers, ec);
    }

    template <typename ConstBufferSequence, typename WriteHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(WriteHandler, void (audio::error_code))
    async_write(implementation_type& impl, const ConstBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(WriteHandler)handler)
    {
        boost::asio::detail::async_result_init<
            WriteHandler, void(audio::error_code, std::size_t)> init(
                BOOST_ASIO_MOVE_CAST(WriteHandler)(handler));

        service_impl_.async_write(impl, buffers, init.handler);

        return init.result.get();
    }
    
    template <typename ConstBufferSequence, typename WriteHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(WriteHandler, void (audio::error_code))
    async_write_some(implementation_type& impl, const ConstBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(WriteHandler)handler)
    {
        boost::asio::detail::async_result_init<
            WriteHandler, void(audio::error_code, std::size_t)> init(
                BOOST_ASIO_MOVE_CAST(WriteHandler)(handler));

        service_impl_.async_write_some(impl, buffers, init.handler);

        return init.result.get();
    }
    
    
    template <typename MutableBufferSequence, typename ReadHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(ReadHandler, void (audio::error_code, std::size_t))
    async_read(implementation_type& impl, const MutableBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(ReadHandler) handler)
    {
        boost::asio::detail::async_result_init<
            ReadHandler, void (audio::error_code, std::size_t)> init(
                BOOST_ASIO_MOVE_CAST(ReadHandler)(handler));

        service_impl_.async_read(impl, buffers, init.handler);

        return init.result.get();
    }
    
    template <typename MutableBufferSequence, typename ReadHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(ReadHandler, void (audio::error_code, std::size_t))
    async_read_some(implementation_type& impl, const MutableBufferSequence& buffers,  BOOST_ASIO_MOVE_ARG(ReadHandler) handler)
    {
        boost::asio::detail::async_result_init<
            ReadHandler, void (audio::error_code, std::size_t)> init(
                BOOST_ASIO_MOVE_CAST(ReadHandler)(handler));

        service_impl_.async_read_some(impl, buffers, init.handler);

        return init.result.get();
    }

  
private:
    /// Destroy all user-defined handler objects owned by the service.
    void shutdown_service()
    {
        service_impl_.shutdown_service();
    }

    /// Specific audio driver implementation. 
    service_impl_type service_impl_;
};

template<typename ...CompileSettings>
using audio_socket_service_t = basic_audio_socket_service_t<detail::audio_device_service<CompileSettings...>>;


} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_AUDIO_SOCKET_SERVICE_HPP
