#ifndef TRIAL_AUDIO_UNITS_HPP
#define TRIAL_AUDIO_UNITS_HPP

#include <boost/asio/detail/config.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace units {



struct Hertz {
    using value_type = unsigned long long int;

    value_type value;

    friend std::ostream& operator<< (std::ostream& stream, const Hertz& hertz) {
        stream << hertz.value << " Hz";
        return stream;
    }

    explicit operator value_type() const {
        return value;
    }
};

struct Byte {
    using value_type = unsigned long long int;

    value_type value;
};

struct Frame {

    using value_type = unsigned long long int;

    value_type value;

    friend std::ostream& operator<< (std::ostream& stream, const Frame& frames) {
        stream << frames.value << " frames";
        return stream;
    }

    explicit operator value_type() const {
        return value;
    }

    Frame operator/(const Frame& rhs)
    {
        return Frame{value / rhs.value};
    }

    Frame operator*(const Frame& rhs)
    {
        return Frame{value * rhs.value};
    }

    Frame operator+(const Frame& rhs)
    {
        return Frame{value + rhs.value};
    }

    Frame operator-(const Frame& rhs)
    {
        return Frame{value - rhs.value};
    }
};

struct Microsecond {
    using value_type = unsigned long long int;

    value_type value;

    friend std::ostream& operator<< (std::ostream& stream, const Microsecond& us) {
        stream << us.value << " us";
        return stream;
    }

    explicit operator value_type() const {
        return value;
    }

    template<typename T>
    Microsecond operator/(const T& rhs)
    {
        return Microsecond{value / rhs};
    }

};


Hertz operator "" _Hz(unsigned long long int freq) { return Hertz{freq}; }


Hertz operator "" _kHz(unsigned long long int freq) { return operator""_Hz(freq*1000); }

Hertz operator "" _kHz(long double freq) { return operator""_Hz(freq*1000); }


Frame operator "" _frames(unsigned long long int frame_span) { return Frame{frame_span}; }


Microsecond operator "" _us(unsigned long long int time_span) { return Microsecond{time_span}; }


Microsecond operator "" _ms(unsigned long long int time_span) { return operator""_us(time_span*1000); }

Microsecond operator "" _ms(long double time_span) { return operator""_us(time_span*1000); }


Microsecond operator "" _s(unsigned long long int time_span) { return operator""_ms(time_span*1000); }

Microsecond operator "" _s(long double time_span) { return operator""_ms(time_span*1000); }

} // namespace units
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_UNITS_HPP