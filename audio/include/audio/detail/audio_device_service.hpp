#ifndef TRIAL_AUDIO_DETAIL_AUDIO_DEVICE_SERVICE_HPP
#define TRIAL_AUDIO_DETAIL_AUDIO_DEVICE_SERVICE_HPP

#include <boost/asio/detail/config.hpp>

#define OOP 1

#if defined(BOOST_ASIO_HAS_IOCP)
#include "audio/detail/win_iocp/audio_device_service.hpp"
#else
#ifndef OOP
#include "audio/detail/reactive/audio_device_service.hpp"
#else
#include "audio/detail/reactive_oop/audio_device_service.hpp"
#include <audio/devices/alsa_device.hpp>
#endif
#endif

namespace trial {
namespace audio {
namespace detail {

template<typename ...CompileSettings>
#if defined(BOOST_ASIO_HAS_IOCP)
using audio_device_service = win_iocp::audio_device_service<CompileSettings...>;
#else
using audio_device_service = reactive::audio_device_service<CompileSettings...>;
#endif

}
}
}

#endif //TRIAL_AUDIO_DETAIL_AUDIO_DEVICE_SERVICE_HPP
