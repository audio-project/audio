#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_REACTOR_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_REACTOR_HPP

#include <boost/asio/detail/config.hpp>

#include <boost/asio/detail/reactor.hpp>


#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {
    
using reactor = boost::asio::detail::reactor;
    
}
} // namespace detail
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_AUDIO_DETAIL_REACTIVE_REACTOR_HPP