#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#if defined(WITH_ALSA)
#include "audio/detail/reactive/handle_ops/alsa_handle_ops.hpp"
#else
#include "audio/detail/reactive/handle_ops/file_handle_ops.hpp"
#endif

namespace trial {
namespace audio {
namespace detail {
namespace reactive {
    
#if defined(WITH_ALSA)
    namespace handle_ops = alsa_handle_ops;
#else
    namespace handle_ops = file_handle_ops;
#endif

}
}
}
}

#endif // TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HPP