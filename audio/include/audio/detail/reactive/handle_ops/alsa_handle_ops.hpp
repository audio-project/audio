#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_ALSA_HANDLE_OPS_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_ALSA_HANDLE_OPS_HPP

#include <boost/mpl/assert.hpp>
#include <boost/variant.hpp>
#include <boost/asio/detail/consuming_buffers.hpp>
#include <boost/asio/detail/socket_types.hpp>
#include <cerrno>
#include <alsa/asoundlib.h>
#include <iostream>
#include <memory>

#include "audio/error_codes.hpp"
#include "audio/util.hpp"
#include "audio/audio_socket_base.hpp"

namespace trial {
namespace audio {
namespace detail {
namespace reactive {
namespace alsa_handle_ops {

using namespace util;

using handle_t = snd_pcm_t*;

using socket_type = boost::asio::detail::socket_type;

using state_type = unsigned char;

using i_2ch_s16_le = BufferFormat<
        options::compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        options::compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_le>,
        options::compile_option_t<options::channels_t , 2>
>;

using i_2ch_s16_be = BufferFormat<
        options::compile_option_t<options::container_format_t, options::container_format_t::values::interleaved>,
        options::compile_option_t<options::sample_format_t, options::sample_format_t::values::signed16_be>,
        options::compile_option_t<options::channels_t , 2>
>;

struct Frames {};
struct Bytes {};

template<typename T>
struct buffer_size;

template<>
struct buffer_size<Frames> {
    std::size_t value;

    buffer_size() = default;

    explicit buffer_size(std::size_t value) : value(value) {}
};

template<>
struct buffer_size<Bytes> {
    std::size_t value;

    explicit buffer_size(std::size_t value) : value(value) {}
};





// Descriptor state bits.
enum
{
  // The user wants a non-blocking descriptor.
  user_set_non_blocking = 1,

  // The descriptor has been set non-blocking.
  internal_non_blocking = 2,

  // Helper "state" used to determine whether the descriptor is non-blocking.
  non_blocking = user_set_non_blocking | internal_non_blocking,

  // The descriptor may have been dup()-ed.
  possible_dup = 4
};

template<typename ReturnType>
inline ReturnType error_wrapper(ReturnType return_value,
                                audio::error_code &ec) {
                                    
    if (errno == ENOTSOCK)
        errno = 0;
    
    ec = audio::error_code(errno, audio::error::get_system_category());

    return return_value;
}


struct AlsaDeleter {
    void operator()(snd_pcm_t* handle) {
        snd_pcm_close(handle);
    }
};



template<typename T>
void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const T &setting) {
    std::cout << "Missing" << std::endl;
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::direction_t &setting) {
    std::cout << "Direction not needed here" << std::endl;
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::device_id_t &setting) {
    std::cout << "Device ID not needed here" << std::endl;
}

struct hw_configure {
    explicit hw_configure(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec) : handle(handle), params(params), ec(ec) {}

    const handle_t &handle;
    snd_pcm_hw_params_t* params;
    audio::error_code &ec;
};

struct latency_configure : public boost::static_visitor<>, hw_configure
{

    using hw_configure::hw_configure;

    void operator()(units::Microsecond microseconds) const {
        std::cout << "Latency size: " << microseconds << std::endl;

        unsigned int value = static_cast<unsigned long long int>(microseconds);
        int dir;

        int err = snd_pcm_hw_params_set_buffer_time_near(handle, params, &value, &dir);
        if(err < 0)
            ec = audio::error::already_open;
    }
    void operator()(units::Frame frames) const {
        std::cout << "Latency size: " << frames << std::endl;

        snd_pcm_uframes_t value = static_cast<unsigned long long int>(frames);
        int dir;

        int err = snd_pcm_hw_params_set_buffer_size_near(handle, params, &value);
        if(err < 0)
            ec = audio::error::already_open;
    }
};

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::latency_t &setting) {
    boost::apply_visitor(latency_configure{handle, params, ec}, setting.value());
}

struct period_configure : public boost::static_visitor<>, hw_configure
{

    using hw_configure::hw_configure;

    void operator()(units::Microsecond microseconds) const {
        std::cout << "Period size: " << microseconds << std::endl;

        unsigned int value = static_cast<unsigned long long int>(microseconds);
        int dir;

        int err = snd_pcm_hw_params_set_period_time_near(handle, params, &value, &dir);
        if (err < 0) {
            ec = audio::error::already_open;
            return;
        }
    }
    void operator()(units::Frame frames) const {
        std::cout << "Period size: " << frames << std::endl;

        snd_pcm_uframes_t value = static_cast<unsigned long long int>(frames);
        int dir;

        int err = snd_pcm_hw_params_set_period_size_near(handle, params, &value, &dir);
        if (err < 0) {
            ec = audio::error::already_open;
            return;
        }
    }
};

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::period_t &setting) {
    boost::apply_visitor(period_configure{handle, params, ec}, setting.value());
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::sample_rate_t &setting) {
    std::cout << "Sample rate: " << setting.value() << std::endl;
    
    unsigned int value = static_cast<unsigned long long int>(setting.value());
    
    int err = snd_pcm_hw_params_set_rate_near(handle, params, &value, 0);
	if(err < 0)
		ec = audio::error::already_open;
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::sample_format_t &setting) {
   
    using enum_t = options::sample_format_t::values ;

    snd_pcm_format_t value;
    
    switch(setting.value()) {
        case enum_t::unsigned16_le: value = SND_PCM_FORMAT_U16_LE;
                                    break;
        case enum_t::signed16_le:      value = SND_PCM_FORMAT_S16_LE ;
                                    break;
        case enum_t::signed16_be: value = SND_PCM_FORMAT_S16_BE;
                                    break;
        default: value = SND_PCM_FORMAT_UNKNOWN;
                    break;
    }
    
    std::cout << "Format: " << value << std::endl;
       
    int err = snd_pcm_hw_params_set_format(handle, params, value);
	if(err < 0)
		ec = audio::error::already_started;
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::container_format_t &setting) {
   
    using enum_t = options::container_format_t::values ;

    snd_pcm_access_t value;
    
    switch(setting.value()) {
        case enum_t::interleaved: value = SND_PCM_ACCESS_RW_INTERLEAVED;
                                    break;
        case enum_t::planar:      value = SND_PCM_ACCESS_RW_NONINTERLEAVED ;
                                    break;
        default: value = SND_PCM_ACCESS_RW_INTERLEAVED;
                    break;
    }
    
    std::cout << "Container format: " << value << std::endl;
       
    int err = snd_pcm_hw_params_set_access(handle, params, value);
	if(err < 0)
		ec = audio::error::already_open;
}



void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::channels_t &setting) {
    std::cout << "Channels: " << setting.value() << std::endl;
    
    unsigned int value = setting.value();
    
    int err = snd_pcm_hw_params_set_channels(handle, params, value);
	if(err < 0)
		ec = audio::error::already_open;
}

void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, const options::resample_t &setting) {
    std::cout << "Alsa may resample: " << setting.value() << std::endl;
    
    int value = (setting.value() ? 1 : 0);
    
    int err = snd_pcm_hw_params_set_rate_resample(handle, params, value);
	if(err < 0)
		ec = audio::error::already_open;
}


void set_hw_param_collection_impl(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec) {
    
}

template<typename T, typename ...Args>
void set_hw_param_collection_impl(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, T setting, Args... args) {
    set_hw_param(handle, params, ec, setting);
    if(ec)
        return;
    set_hw_param_collection_impl(handle, params, ec, args...);
}


template<typename ...Args>
void set_hw_param_collection(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec, Args... args) {
    set_hw_param_collection_impl(handle, params, ec, args...);
    
    int err = snd_pcm_hw_params(handle, params);
	if (err < 0) {
		ec = audio::error::already_open;
	}
}

template<typename ...Settings>
struct Config;

template<typename T, typename ...Settings>
struct Config<T, Settings...> : Config<Settings...> {
    static void set_hw_param_impl(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec) {
        std::cout << "Compile setting" << std::endl;
        auto value = T::value();
        set_hw_param(handle, params, ec, value);
        Config<Settings...>::set_hw_param_impl(handle, params, ec);
    }
};

template<>
struct Config<> {
    static void set_hw_param_impl(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec) {};
};




template<typename Device, typename ...Args>
void configure_device(const handle_t &handle, audio::error_code &ec, Args... args) {
    int err;
    
    snd_pcm_hw_params_t *hw_params;
    snd_pcm_hw_params_alloca(&hw_params);
    
    snd_pcm_sw_params_t* sw_params;
    snd_pcm_sw_params_alloca(&sw_params);

    err = snd_pcm_hw_params_any(handle, hw_params);

	if (err < 0) {
        ec = audio::error::already_open;
        return;
	}

    Device::set_hw_param(handle, hw_params, ec);

    //Device::has_settings(args...);

    set_hw_param_collection(handle, hw_params, ec, args...);




    if(ec)
        return;
    
            
    long unsigned int buffer_size;
    snd_pcm_hw_params_get_buffer_size(hw_params, &buffer_size);
    std::cout << "Buffer Size: " << buffer_size << std::endl;
    
    long unsigned int period_size;
    snd_pcm_hw_params_get_period_size(hw_params, &period_size, NULL);
    std::cout << "Period Size: " << period_size << std::endl;
	
	err = snd_pcm_sw_params_current(handle, sw_params);
	
	err = snd_pcm_sw_params_set_start_threshold(handle, sw_params, (buffer_size/period_size)*period_size);
   
	
	err = snd_pcm_sw_params_set_avail_min(handle, sw_params, period_size);
	
	err = snd_pcm_sw_params(handle, sw_params);
    
    err = snd_pcm_prepare(handle);
    if (err < 0) {
		ec = audio::error::already_open;
		return;
	}
}


template<typename ...Settings>
struct device_t {

    static void set_hw_param(const handle_t &handle, snd_pcm_hw_params_t* params, audio::error_code &ec) {
        Config<Settings...>::set_hw_param_impl(handle, params, ec);
    }

    using handle_t = handle_t;


    static const unsigned int frame_size = 4;


    device_t() : handle(nullptr) {};

    device_t(const handle_t &handle) : handle(handle) {}
    device_t(handle_t&& handle) : handle(std::move(handle)) {}

    device_t(const device_t &) = default;
    device_t& operator=(const device_t &) = default;

    device_t(device_t&& other) : handle(std::move(other.handle)) {
        other.handle = nullptr;
    }

    device_t& operator=(device_t&& other) {
        this->handle = std::move(other.handle);
        other.handle = nullptr;
        return *this;
    }

    handle_t get_handle() const {
        return handle;
    }

private:
    handle_t handle;
};



std::unique_ptr<snd_pcm_t, AlsaDeleter> open_device(const options::device_id_t& device_id, const options::direction_t& direction, audio::error_code &ec) {

    handle_t handle;

    const char* device_id_ = device_id.value().c_str();
    snd_pcm_stream_t direction_ = (direction.value() == options::direction_t::values::playback ? SND_PCM_STREAM_PLAYBACK : SND_PCM_STREAM_CAPTURE);
    
    int err = error_wrapper(
                snd_pcm_open(
                    &handle,
                    device_id_,
                    direction_,
                    SND_PCM_NONBLOCK
                ),
                ec
            );

    if(err < 0)
        ec = audio::error::already_open;
    
    return std::unique_ptr<snd_pcm_t, AlsaDeleter>(handle);
}

// API methods

template<typename Device, typename ...Args>
auto open(audio::error_code &ec, Args... args) -> Device {

    auto device_id = util::get_runtime_setting<options::device_id_t>( args...);
    auto direction = util::get_runtime_setting<options::direction_t>( args...);

    std::cout << device_id.value() << std::endl;

    auto handle = open_device(device_id, direction, ec);
    if(ec)
        return Device(nullptr);
    
    configure_device<Device>(handle.get(), ec, args...);
    if(ec)
        return Device(nullptr);
    
    return Device(handle.release());
}

template<typename Device>
bool is_open(const Device &device) {
    return device.get_handle() != nullptr;
}

template<typename Device>
void close(const Device &device) {
    if (is_open(device))
        snd_pcm_close(device.get_handle());
}

template <typename Device, typename ConstBufferSequence>
std::size_t sync_write(const Device &device, const ConstBufferSequence &buffers, audio::error_code &ec) {
    using pcm_format = typename ConstBufferSequence::pcm_format;

    //static_assert(Device::template has_format<pcm_format>::value, "The device does not support this buffer");

    return sync_write_dispatch(device, buffers, ec, pcm_format{});
};

template <typename Device, typename MutableBufferSequence>
std::size_t sync_read(const Device &device, const MutableBufferSequence &buffers, audio::error_code &ec) {
    using pcm_format = typename MutableBufferSequence::pcm_format;

    //static_assert(Device::template has_format<pcm_format>::value, "The device does not support this buffer");

    return sync_read_dispatch(device, buffers, ec, pcm_format{});
};



template <typename Device, typename ConstBufferSequence>
bool non_blocking_write(const Device &device, const ConstBufferSequence &buffers,
                        audio::error_code &ec, std::size_t &bytes_transferred) {

    using pcm_format = typename ConstBufferSequence::pcm_format;

    //static_assert(Device::template has_format<pcm_format>::value, "The device does not support this buffer");

    return non_blocking_write_dispatch(device, buffers, ec, bytes_transferred, pcm_format{});
};

template <typename Device, typename ConstBufferSequence, typename Buffer>
bool non_blocking_write(const Device &device, const boost::asio::detail::consuming_buffers<Buffer, ConstBufferSequence> &buffers,
                        audio::error_code &ec, std::size_t &bytes_transferred) {

    using pcm_format = typename ConstBufferSequence::pcm_format;

    //static_assert(Device::template has_format<pcm_format>::value, "The device does not support this buffer");

    return non_blocking_write_dispatch(device, *buffers.begin(), ec, bytes_transferred, pcm_format{});
};



// Non API methods

template <typename Device, typename ConstBufferSequence>
std::size_t sync_write_dispatch(const Device &device, const ConstBufferSequence &buffers, audio::error_code &ec, i_2ch_s16_le) {

    std::cout << "Interleaved Signed 16-bit Little Endian" << std::endl;
    return alsa_sync_write(device, boost::asio::detail::buffer_cast_helper(buffers),  boost::asio::detail::buffer_size_helper(buffers), ec);
};


template <typename Device, typename ConstBufferSequence>
std::size_t sync_write_dispatch(const Device &device, const ConstBufferSequence &buffers, audio::error_code &ec, i_2ch_s16_be) {

    std::cout << "Interleaved Signed 16-bit Big Endian" << std::endl;
    return alsa_sync_write(device, boost::asio::detail::buffer_cast_helper(buffers),  boost::asio::detail::buffer_size_helper(buffers), ec);
};

template<typename Device>
std::size_t alsa_sync_write(const Device &device, const void *buf,
                       std::size_t length, audio::error_code &ec) {
    // Change PCM handle to blocking mode. 
    
    auto handle = device.get_handle();
    int result_fix_1 = snd_pcm_nonblock(handle, 0);

    // Prepare the PCM handle.
    int result_fix_2 = snd_pcm_prepare(handle);

    // Write buffer to ALSA.
    long frames = error_wrapper(snd_pcm_writei(handle, buf, length), ec);
    if (frames < 0) {
        snd_pcm_nonblock(handle, 1);
        return 0;
    }

    snd_pcm_nonblock(handle, 1);
    ec = audio::error_code();
    return frames;
}


template <typename Device, typename MutableBufferSequence>
std::size_t sync_read_dispatch(const Device &device, const MutableBufferSequence &buffers, audio::error_code &ec, i_2ch_s16_le) {

    std::cout << "Interleaved Signed 16-bit Little Endian" << std::endl;
    return alsa_sync_read(device, boost::asio::detail::buffer_cast_helper(buffers),  boost::asio::detail::buffer_size_helper(buffers), ec);
};


template <typename Device, typename MutableBufferSequence>
std::size_t sync_read_dispatch(const Device &device, const MutableBufferSequence &buffers, audio::error_code &ec, i_2ch_s16_be) {

    std::cout << "Interleaved Signed 16-bit Big Endian" << std::endl;
    return alsa_sync_read(device, boost::asio::detail::buffer_cast_helper(buffers),  boost::asio::detail::buffer_size_helper(buffers), ec);
};


//TODO NON BLOCK + ERROR HANDLING
template<typename Device>
std::size_t alsa_sync_read(const Device &device, void *buf,
                      std::size_t length, audio::error_code &ec) {
    auto handle = device.get_handle();
    // Change PCM handle to blocking mode.
    int result_fix_1 = snd_pcm_nonblock(handle, 0);

    long frames = error_wrapper(snd_pcm_readi(handle, buf, length), ec);

    if (frames < 0) {
        snd_pcm_nonblock(handle, 1);
        return 0;
    }

    snd_pcm_nonblock(handle, 1);
    ec = audio::error_code();
    return frames;
}



template<typename Device, typename ConstBufferSequence>
bool non_blocking_write_dispatch(const Device &device, const ConstBufferSequence& buffer,
                        audio::error_code &ec, std::size_t &bytes_transferred,
                        i_2ch_s16_le) {

    buffer_size<Frames> frames;
    bool result = non_blocking_write(device, boost::asio::detail::buffer_cast_helper(buffer), buffer_size<Frames>(boost::asio::detail::buffer_size_helper(buffer)/device.frame_size), ec, frames);
    bytes_transferred = frames.value * device.frame_size;
    return result;
}


template<typename Device, typename ConstBufferSequence>
bool non_blocking_write_dispatch(const Device &device, const ConstBufferSequence& buffer,
                        audio::error_code &ec, std::size_t &bytes_transferred,
                        i_2ch_s16_be) {

    buffer_size<Frames> frames;
    bool result = non_blocking_write(device, boost::asio::detail::buffer_cast_helper(buffer), buffer_size<Frames>(boost::asio::detail::buffer_size_helper(buffer)/Device::frame_size), ec, frames);
    bytes_transferred = frames.value * device.frame_size;
    return result;
}


template<typename Device>
bool non_blocking_write(const Device &device, const void *buf,
                        buffer_size<Frames> length, audio::error_code &ec,
                        buffer_size<Frames> &frames_transfered) {

    auto handle = device.get_handle();
    
    long frames = error_wrapper(snd_pcm_writei(handle, buf, length.value), ec);

    if (frames > 0) {
        ec = audio::error_code();
        frames_transfered.value = frames;
    } else {
        frames_transfered.value = 0;
        return false;
    }

    return true;
}


template<typename Device>
bool non_blocking_read(const Device &device, void *buf,
                       buffer_size<Frames> length, audio::error_code &ec,
                       buffer_size<Frames> &frames_transferred) {   

    auto handle = device.get_handle();
    long frames = error_wrapper(snd_pcm_readi(handle, buf, length.value), ec);
    
    if (frames > 0) {
        ec = audio::error_code();
        frames_transferred.value = frames;
    } else {
        frames_transferred.value = 0;
        return false;
    }

    return true;
}


template<typename Device>
bool non_blocking_read(const Device &device, void *buf,
                       std::size_t length, audio::error_code &ec,
                       std::size_t &bytes_transferred) {
    buffer_size<Frames> frames;
    bool result = non_blocking_read(device, buf, buffer_size<Frames>(length/4), ec, frames);
    bytes_transferred = frames.value * 4;
    return result;
}

template<typename Device>
socket_type poll_read(const Device &device, audio::error_code &ec) {
    auto handle = device.get_handle();
    int count = snd_pcm_poll_descriptors_count(handle);
    if (count == 0) {
        return -1;
    }

    struct pollfd ufds[count];

    int result = error_wrapper(snd_pcm_poll_descriptors(handle, ufds, count), ec);
    if (result < 0) {
        return -1;
    }

    // DO READ
    return ufds[0].fd;
}


template<typename Device>
bool set_internal_non_blocking(const Device &device, state_type& state,
                        bool value, audio::error_code& ec) {

                        auto handle = device.get_handle();
    if (handle < 0)
    {
        ec = audio::error::bad_descriptor;
        return false;
    }

    if (!value && (state & user_set_non_blocking))
    {
        // It does not make sense to clear the internal non-blocking flag if the
        // user still wants non-blocking behaviour. Return an error and let the
        // caller figure out whether to update the user-set non-blocking flag.
        ec = audio::error::invalid_argument;
        return false;
    }

    errno = 0;
    
    // FIX ME
    auto result = 1;

    if (result >= 0)
    {
        ec = audio::error_code();
        if (value)
            state |= internal_non_blocking;
        else
            state &= ~internal_non_blocking;
        return true;
    }

    return false;
                        
}

} // namespace handle_ops
} // namespace reactive
} // namespace detail
} // namespace audio
} // namespace trial

#endif //TRIAL_AUDIO_DETAIL_REACTIVE_ALSA_HANDLE_OPS_HPP