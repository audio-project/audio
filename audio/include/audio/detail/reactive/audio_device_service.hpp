#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_device_SERVICE_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_device_SERVICE_HPP

#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/detail/addressof.hpp>
#include <boost/asio/detail/bind_handler.hpp>
#include <boost/asio/detail/buffer_sequence_adapter.hpp>
#include <boost/asio/detail/fenced_block.hpp>
#include <boost/asio/detail/noncopyable.hpp>
#include <boost/asio/detail/socket_types.hpp>

#include <poll.h>

#include "audio/detail/config.hpp"
#include "audio/error_codes.hpp"
#include "audio/detail/reactive/reactor.hpp"
#include "audio/detail/reactive/handle_ops.hpp"
#include "audio/detail/reactive/handler/read_op.hpp"
#include "audio/detail/reactive/handler/write_op.hpp"
#include "audio/detail/reactive/handler/null_buffers_op.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {

namespace device_ops {
using namespace handle_ops;
}

template <typename... Settings>
class audio_device_service  {
public:
    // The native type of a descriptor.
    using device_t = device_ops::device_t<Settings...>;

    using handle_t = typename device_t::handle_t;

    using descriptor_t = boost::asio::detail::socket_type;

    struct device_data_type : private boost::asio::detail::noncopyable {
    public:

        explicit  device_data_type(device_t&& device) : device_(std::move(device)) {}

        explicit device_data_type(const device_t& device) : device_data_type(std::move(device_t{device})) {}

        device_data_type() : device_data_type(std::move(device_t{})) {}

        device_data_type(device_data_type&& other) : device_(std::move(other.device_)) {

        }

        device_data_type& operator=(device_data_type&& other) {
            auto tmp = device_data_type{std::move(this->device_)};
            this->device_ = std::move(other.device_);
            return *this;
        }

        ~device_data_type() {
           if (device_ops::is_open(device_))
                device_ops::close(device_);

        }

        device_t release() {
            return std::move(device_);
        }

        device_t device() const {
            return device_;
        }

        device_t device() {
            return device_;
        }
    private:
        device_t device_;

    };

    struct descriptor_data_type : private boost::asio::detail::noncopyable {
    public:
        explicit descriptor_data_type(descriptor_t &&descriptor) : state_(0), reactor_data_(), descriptor_(std::move(descriptor)) {}

        descriptor_data_type() : descriptor_data_type(std::move(descriptor_t{-1})) {}

        descriptor_data_type(descriptor_data_type&& other) : state_(std::move(other.state_)), reactor_data_(std::move(other.reactor_data_)), descriptor_(std::move(other.descriptor_))
        {
            auto tmp = descriptor_data_type{};
            std::swap(other.state_, tmp.state_);
            std::swap(other.reactor_data_, tmp.reactor_data_);
            std::swap(other.descriptor_, tmp.descriptor_);
        }

        descriptor_data_type(descriptor_data_type&& other, reactor &reactor_ ) : state_(std::move(other.state_)), reactor_data_(), descriptor_(std::move(other.descriptor_))
        {
            reactor_.move_descriptor(this->descriptor_, this->reactor_data_, other.reactor_data_);

            auto tmp = descriptor_data_type{};
            std::swap(other.state_, tmp.state_);
            std::swap(other.reactor_data_, tmp.reactor_data_);
            std::swap(other.descriptor_, tmp.descriptor_);
        }

        descriptor_data_type& operator=(descriptor_data_type&& other)
        {
            auto tmp = descriptor_data_type{std::move(other)};
            std::swap(this->state_, tmp.state_);
            std::swap(this->reactor_data_, tmp.reactor_data_);
            std::swap(this->descriptor_, tmp.descriptor_);
            return *this;
        }

        // The current state of the descriptor.
        device_ops::state_type state_;

        // Per-descriptor data used by the reactor.
        reactor::per_descriptor_data reactor_data_;

        // The native descriptor representation.
        descriptor_t descriptor_;

    };

    // The implementation type of the descriptor.
    class implementation_type : private boost::asio::detail::noncopyable {
    public:

        explicit implementation_type(device_t&& device) : descriptor_data_(), device_data_(std::move(device)) {}

        explicit implementation_type(const device_t& device) : implementation_type(std::move(device_t{device})) {};

        implementation_type() : implementation_type(device_t{}) {}

        implementation_type(implementation_type &&other, reactor &reactor_) : descriptor_data_(std::move(other.descriptor_data_), reactor_), device_data_(std::move(other.device_data_))
        {

        }

        implementation_type& operator=(implementation_type &&other) {
            this->device_data_ = std::move(other.device_data_);
            this->descriptor_data_ = std::move(other.descriptor_data_);
            return *this;
        }

    private:
        // Only this service will have access to the internal values.
        friend class audio_device_service<Settings...>;

        descriptor_data_type descriptor_data_;

        // The native device representation.
        device_data_type device_data_;

        device_t device() const {
            return device_data_.device();
        }

        device_t device() {
            return device_data_.device();
        }
    };

private:

    /// Determine whether the device is open.
    inline bool is_open_(const device_data_type &device_data) const {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "is_open"));

        return device_ops::is_open(device_data.device());
    }

    /// Determine whether the device is open.
    inline bool is_registered_(const descriptor_data_type &descriptor_data) const {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "is_registered"));

        return descriptor_data.descriptor_ != -1;
    }

    /// Get the native device implementation.
    inline device_t get_(const device_data_type &device_data) const {
        return device_data.device();
    }

    inline audio::error_code get_descriptor_(const device_data_type& device_data, descriptor_t& descriptor, audio::error_code& ec) {
        descriptor = device_ops::poll_read(device_data.device(), ec);
        return ec;
    }


    descriptor_t release_(descriptor_data_type &descriptor_data) {
        auto tmp = descriptor_data_type{std::move(descriptor_data)};
        if (is_registered_(tmp))
            reactor_.deregister_descriptor(tmp.descriptor_, tmp.reactor_data_, false);

        return std::move(tmp.descriptor_);
    }

    audio::error_code reset_(descriptor_data_type &descriptor_data, descriptor_t &descriptor, audio::error_code &ec) {

        auto tmp = descriptor_data_type{std::move(descriptor)};

        if(tmp.descriptor_ >= 0) {
            if (auto err = reactor_.register_descriptor(tmp.descriptor_, tmp.reactor_data_)) {
                 ec = audio::error_code(err, audio::error::get_system_category());
                 descriptor = std::move(tmp.descriptor_);
                 return ec;
            }


        }

        if (release_(descriptor_data) != -1)
            tmp.state_ = device_ops::possible_dup; //?

        descriptor_data = std::move(tmp);

        return ec;
    }


    device_data_type release_(implementation_type &impl, audio::error_code &ec) {

        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "release"));

        release_(impl.descriptor_data_);

        return std::move(impl.device_data_);
    }

    audio::error_code reset_(implementation_type &impl, device_data_type &&device_data, audio::error_code &ec) {

        auto tmp = device_data_type{std::move(device_data)};

        auto descriptor = descriptor_t{-1};

        if (is_open_(tmp)) {
            if (get_descriptor_(tmp, descriptor, ec))
            {
                device_data = std::move(tmp);
                return ec;
            }
        }

        if (reset_(impl.descriptor_data_, descriptor, ec))
        {
            device_data = std::move(tmp);
            return ec;
        }

        impl.device_data_ = std::move(tmp);

        return ec;

    }

    audio::error_code reset_(implementation_type &impl, audio::error_code &ec) {
        return reset_(impl, device_data_type{}, ec);
    }


    /// Open an audio socket implementation.
    template <typename... Args>
    audio::error_code open_(device_data_type& device_data, audio::error_code &ec, Args... args) {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "open"));
        auto tmp = device_data_type{std::move(device_ops::open<device_t>(ec, args...))};
        if(ec)
            return ec;

        device_data = std::move(tmp);
        return ec;
    }

    /// Cancel all asynchronous operations associated with the device.
    audio::error_code cancel_(descriptor_data_type &descriptor_data, audio::error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("descriptor", &impl, "cancel"));

        if (!is_registered_(descriptor_data)) {
            ec = audio::error::bad_descriptor;
            return ec;
        }

        reactor_.cancel_ops(descriptor_data.descriptor_, descriptor_data.reactor_data_);

        return ec;
    }



public:

    // Constructor.
    audio_device_service(boost::asio::io_service &io_service) : reactor_(boost::asio::use_service<reactor>(io_service))
    {
        reactor_.init_task();
    }

    // Destroy all user-defined handler objects owned by the service.
    void shutdown_service() {}

    // Construct a new descriptor implementation.
    void construct(implementation_type &impl) {
        impl = std::move(implementation_type{});
    }

    // Move-construct a new descriptor implementation.
    void move_construct(implementation_type &impl, implementation_type &other_impl)
    {
        impl = std::move(implementation_type{other_impl, reactor_});
    }

    // Destroy a descriptor implementation.
    void destroy(implementation_type &impl)
    {
        audio::error_code ignored_ec;
        reset_(impl, ignored_ec);
    }

    bool is_open(const implementation_type &impl) const {
        return is_open_(impl.device_data_);
    }

    /// Open an audio socket implementation.
    template <typename... Args>
    audio::error_code open(implementation_type &impl, audio::error_code &ec,
                           Args... args) {

        /// Change to a constructor of device_t
        auto device_data = device_data_type{};

        if (open_(device_data, ec, args...))
            return ec;

        reset_(impl, std::move(device_data), ec);

        return ec;
    }


    /// Get the native device implementation.
    device_t native_device(const implementation_type &impl) const {
        return get_(impl.device_data_);
    }

    /// Get the native device implementation.
    handle_t native_handle(const implementation_type &impl) const {
        return impl.device_data_.device().get_handle();
    }

    template <typename... RuntimeSettings>
    audio::error_code set_options(implementation_type &impl, audio::error_code &ec, RuntimeSettings... runtime_settings) {
        return ec;
    }



    audio::error_code assign(implementation_type &impl, device_t &&device, audio::error_code &ec) {

        auto device_data = device_data_type{std::move(device)};
        if (reset_(impl, std::move(device_data), ec)) {
            device = std::move(device_data.release());
        }
        return ec;

    }

    /// Assign an existing native device to an audio socket.
    audio::error_code assign(implementation_type &impl, const device_t &device, audio::error_code &ec) {

        return assign(impl, std::move(device_t{device}), ec);

    }

    /// Move-assign from another descriptor implementation.
    void move_assign(implementation_type &impl,
                     audio_device_service &other_service,
                                     implementation_type &other_impl)
    {
        release_(impl);
        impl = std::move(implementation_type{other_impl, other_service.reactor_});
    }

    device_t release(implementation_type &impl) {
        auto device_data = std::move(release(impl));
        return std::move(device_data.release());
    }



    /// Close an audio socket implementation.
    audio::error_code close(implementation_type &impl, audio::error_code &ec) {

        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "close"));

        return reset_(impl, ec);

    }


    /// Cancel all asynchronous operations associated with the device.
    audio::error_code cancel(implementation_type &impl, audio::error_code &ec) {
        return cancel_(impl.descriptor_data_, ec);
    }

    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write(implementation_type &impl,
                      const ConstBufferSequence &buffers, error_code &ec) {
        return write_some(impl, buffers, ec);
    }

    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write_some(implementation_type &impl,
                           const ConstBufferSequence &buffers, error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "sync_write"));

        return device_ops::sync_write(impl.device(), buffers, ec);
    }

    /// Start an asynchronous write. The data being sent must be valid for the
    /// lifetime of the asynchronous operation.
    template <typename ConstBufferSequence, typename Handler>
    void async_write(implementation_type &impl,
                     const ConstBufferSequence &buffers, Handler &handler) {
        async_write_some(impl, buffers, handler);
    }

    /// Start an asynchronous wait until data can be written without blocking.
    template <typename Handler>
    void async_write(implementation_type &impl,
                     const boost::asio::null_buffers &buffers,
                     Handler &handler) {
        async_write_some(impl, buffers, handler);
    }

    /// Start an asynchronous write. The data being sent must be valid for the
    /// lifetime of the asynchronous operation.
    template <typename ConstBufferSequence, typename Handler>
    void async_write_some(implementation_type &impl,
                          const ConstBufferSequence &buffers,
                          Handler &handler) {
        bool is_continuation =
            boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::write_op<device_t, ConstBufferSequence, Handler> op;
        typename op::ptr p = {
            boost::asio::detail::addressof(handler),
            boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(impl.device(), buffers, handler);

        BOOST_ASIO_HANDLER_CREATION((p.p, "device", &impl, "async_write_some"));

        start_op(impl, reactor::read_op, p.p, is_continuation, true,
                 boost::asio::detail::buffer_sequence_adapter<
                     boost::asio::const_buffer,
                     ConstBufferSequence>::all_empty(buffers));

        p.v = p.p = 0;
    }

    /// Start an asynchronous wait until data can be written without blocking.
    template <typename Handler>
    void async_write_some(implementation_type &impl,
                          const boost::asio::null_buffers &, Handler &handler) {
        bool is_continuation =
            boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::null_buffers_op<device_t, Handler> op;
        typename op::ptr p = {
            boost::asio::detail::addressof(handler),
            boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};

        p.p = new (p.v) op(handler);

        BOOST_ASIO_HANDLER_CREATION(
            (p.p, "device", &impl, "async_write_some(null_buffers)"));

        start_op(impl, reactor::write_op, p.p, is_continuation, false, false);

        p.v = p.p = 0;
    }

    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read(implementation_type &impl,
                     const MutableBufferSequence &buffers, error_code &ec) {
        return read_some(impl, buffers, ec);
    }

    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read_some(implementation_type &impl,
                          const MutableBufferSequence &buffers,
                          error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "sync_read"));

        return device_ops::sync_read(
            impl.device(), boost::asio::detail::buffer_cast_helper(buffers),
            boost::asio::detail::buffer_size_helper(buffers), ec);
    }

    // Start an asynchronous read. The buffer for the data being read must be
    // valid for the lifetime of the asynchronous operation.
    template <typename MutableBufferSequence, typename Handler>
    void async_read(implementation_type &impl,
                    const MutableBufferSequence &buffers, Handler &handler) {
        async_read_some(impl, buffers, handler);
    }

    // Wait until data can be read without blocking.
    template <typename Handler>
    void async_read(implementation_type &impl,
                    const boost::asio::null_buffers &buffers,
                    Handler &handler) {
        async_read_some(impl, buffers, handler);
    }

    // Start an asynchronous read. The buffer for the data being read must be
    // valid for the lifetime of the asynchronous operation.
    template <typename MutableBufferSequence, typename Handler>
    void async_read_some(implementation_type &impl,
                         const MutableBufferSequence &buffers,
                         Handler &handler) {
        bool is_continuation =
            boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::read_op<device_t, MutableBufferSequence, Handler> op;
        typename op::ptr p = {
            boost::asio::detail::addressof(handler),
            boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(impl.device(), buffers, handler);

        BOOST_ASIO_HANDLER_CREATION((p.p, "device", &impl, "async_read_some"));

        start_op(impl, reactor::read_op, p.p, is_continuation, true,
                 boost::asio::detail::buffer_sequence_adapter<
                     boost::asio::mutable_buffer,
                     MutableBufferSequence>::all_empty(buffers));

        p.v = p.p = 0;
    }

    // Wait until data can be read without blocking.
    template <typename Handler>
    void async_read_some(implementation_type &impl,
                         const boost::asio::null_buffers &, Handler &handler) {
        bool is_continuation =
            boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::null_buffers_op<device_t, Handler> op;
        typename op::ptr p = {
            boost::asio::detail::addressof(handler),
            boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(handler);

        BOOST_ASIO_HANDLER_CREATION(
            (p.p, "device", &impl, "async_read_some(null_buffers)"));

        start_op(impl, reactor::read_op, p.p, is_continuation, false, false);

        p.v = p.p = 0;
    }

private:
    void start_op(implementation_type &impl, int op_type,
                                  boost::asio::detail::reactor_op *op,
                                  bool is_continuation, bool is_non_blocking,
                                  bool noop)
    {
        if (!noop) {
            if ((impl.descriptor_data_.state_ & device_ops::non_blocking) ||
                device_ops::set_internal_non_blocking(impl.device(), impl.descriptor_data_.state_, true,
                                                      op->ec_)) {
                reactor_.start_op(op_type, impl.descriptor_data_.descriptor_, impl.descriptor_data_.reactor_data_, op,
                                  is_continuation, is_non_blocking);
                return;
            }
        }

        reactor_.post_immediate_completion(op, is_continuation);
    }

    reactor &reactor_;
};

} // namespace reactive
} // namespace detail
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>



#endif // TRIAL_AUDIO_DETAIL_REACTIVE_device_SERVICE_HPP