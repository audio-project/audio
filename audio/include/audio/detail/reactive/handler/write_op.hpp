#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_WRITE_OP_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_WRITE_OP_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <boost/asio/detail/config.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/detail/addressof.hpp>
#include <boost/asio/detail/bind_handler.hpp>
#include <boost/asio/detail/buffer_sequence_adapter.hpp>
#include <boost/asio/detail/fenced_block.hpp>
#include <boost/asio/detail/reactor_op.hpp>

#include "audio/error_codes.hpp"
#include "audio/detail/reactive/handle_ops.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {
namespace handler {

template<typename Device, typename ConstBufferSequence>
class write_op_base
        : public boost::asio::detail::reactor_op {
public:

    using device_t = Device;

    write_op_base(device_t device, const ConstBufferSequence &buffers,
                       func_type complete_func)
            : reactor_op(&write_op_base::do_perform, complete_func),
              device_(device), buffers_(buffers) {}

    static bool do_perform(reactor_op *base) {
        write_op_base *o(static_cast<write_op_base *>(base));

        return handle_ops::non_blocking_write(o->device_,
                                            o->buffers_,
                                            o->ec_,
                                            o->bytes_transferred_);
    }

private:
    device_t device_;
    ConstBufferSequence buffers_;
};

template<typename Device, typename ConstBufferSequence, typename Handler>
class write_op
        : public write_op_base<Device, ConstBufferSequence> {
public:

    using device_t = typename write_op_base<Device, ConstBufferSequence>::device_t;

    BOOST_ASIO_DEFINE_HANDLER_PTR(write_op);

    write_op(device_t device,
                  const ConstBufferSequence &buffers,
                  Handler &handler)
            : write_op_base<Device, ConstBufferSequence>(device, buffers, &write_op::do_complete),
              handler_(BOOST_ASIO_MOVE_CAST(Handler)(handler)) {
    }

    static void do_complete(boost::asio::detail::io_service_impl *owner,
                            boost::asio::detail::operation *base,
                            const audio::error_code &ec,
                            std::size_t size) {
        write_op * o(static_cast<write_op *>(base));
        ptr p = {boost::asio::detail::addressof(o->handler_), o, o};

        boost::asio::detail::binder2 <Handler,
        audio::error_code,
        std::size_t> handler(o->handler_,
                             o->ec_,
                             o->bytes_transferred_);

        p.h = boost::asio::detail::addressof(handler.handler_);
        p.reset();

        if (owner) {
            boost::asio::detail::fenced_block b(boost::asio::detail::fenced_block::half);
            BOOST_ASIO_HANDLER_INVOCATION_BEGIN((handler.arg1_, handler.arg2_));
            boost_asio_handler_invoke_helpers::invoke(handler, handler.handler_);
            BOOST_ASIO_HANDLER_INVOCATION_END;
        }
    }

private:
    Handler handler_;

};

}
}
} // namespace detail
} // namespace audio
} // namespace trial

#endif // TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_WRITE_OP_HPP