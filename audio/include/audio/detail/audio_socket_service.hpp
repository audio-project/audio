#ifndef TRIAL_AUDIO_DETAIL_AUDIO_SOCKET_SERVICE_HPP
#define TRIAL_AUDIO_DETAIL_AUDIO_SOCKET_SERVICE_HPP

#include <boost/asio/detail/config.hpp>

#include <boost/asio/io_service.hpp>

#include "audio/error.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {

template<typename AudioDeviceService>
class audio_socket_service {
public:

    typedef AudioDeviceService device_service_t;

    typedef typename device_service_t::device_t device_t;

    typedef typename device_service_t::handle_t handle_t;

    typedef typename device_service_t::implementation_type implementation_type;

    audio_socket_service (boost::asio::io_service &io_service): device_service_(io_service)
    {
    }



    /// Construct an audio socket implementation.
    void construct(implementation_type &impl) {
        device_service_.construct(impl);
    }

    /// Destroy an audio socket implementation.
    void destroy(implementation_type &impl) {
        device_service_.destroy(impl);
    }


    // Destroy all user-defined handler objects owned by the service.
    void shutdown_service()
    {
        device_service_.shutdown_service();
    }

    /// Open an audio socket implementation.
    template<typename ...Args>
    device_t create_device(audio::error_code& ec, Args... args)
    {
        return device_service_.create_device(ec, args...);
    }

    /// Open an audio socket implementation.
    template <typename ...Args>
    audio::error_code open(implementation_type &impl,
                           audio::error_code &ec, Args... args) {
        return device_service_.open(impl, ec, args...);
    }

    /// Assign an existing native handle to an audio socket.
    audio::error_code assign(implementation_type &impl,
                             const device_t &audio_device, audio::error_code &ec) {
        return device_service_.assign(impl, audio_device, ec);
    }


    /// Determine whether the handle is open.
    bool is_open(const implementation_type &impl) const {
        return device_service_.is_open(impl);
    }

    /// Close an audio socket implementation.
    audio::error_code close(implementation_type &impl,
                            audio::error_code &ec) {
        return device_service_.close(impl, ec);
    }

    /// Get the native handle implementation.
    device_t native_device(const implementation_type &impl) const {
        return device_service_.native_device(impl);
    }

    /// Get the native handle implementation.
    handle_t native_handle(const implementation_type &impl) const {
        return device_service_.native_handle(impl);
    }


    /// Cancel all asynchronous operations associated with the handle.
    audio::error_code cancel(implementation_type& impl,
                             audio::error_code& ec)
    {
        return device_service_.cancel(impl, ec);
    }

    template <typename... RuntimeSettings>
    audio::error_code set_options(implementation_type &impl, audio::error_code &ec, RuntimeSettings... runtime_settings) {
        return device_service_.set_options(impl, ec, runtime_settings...);
    }


    /// Write to an audio socket implementation.
    template<typename ConstBufferSequence>
    std::size_t write(implementation_type &impl,
                      const ConstBufferSequence &buffers, audio::error_code &ec) {
        return device_service_.write(impl, buffers, ec);
    }

    /// Write some to an audio socket implementation.
    template<typename ConstBufferSequence>
    std::size_t write_some(implementation_type &impl,
                           const ConstBufferSequence &buffers, audio::error_code &ec) {
        return device_service_.write_some(impl, buffers, ec);
    }

    /// Read from an audio socket implementation.
    template<typename MutableBufferSequence>
    std::size_t read(implementation_type &impl,
                     const MutableBufferSequence &buffers, audio::error_code &ec) {
        return device_service_.read(impl, buffers, ec);
    }

    /// Read from an audio socket implementation.
    template<typename MutableBufferSequence>
    std::size_t read_some(implementation_type &impl,
                          const MutableBufferSequence &buffers, audio::error_code &ec) {
        return device_service_.read_some(impl, buffers, ec);
    }

    template<typename ConstBufferSequence, typename Handler>
    void async_write(implementation_type &impl, const ConstBufferSequence &buffers, Handler &handler) {
        device_service_.async_write(impl, buffers, handler);
    }

    template<typename ConstBufferSequence, typename Handler>
    void async_write_some(implementation_type &impl, const ConstBufferSequence &buffers, Handler &handler) {
        device_service_.async_write_some(impl, buffers, handler);
    }

    template<typename MutableBufferSequence, typename Handler>
    void async_read(implementation_type &impl,
                    const MutableBufferSequence &buffers, Handler &handler) {
        device_service_.async_read(impl, buffers, handler);
    }

    template<typename MutableBufferSequence, typename Handler>
    void async_read_some(implementation_type &impl,
                         const MutableBufferSequence &buffers, Handler &handler) {
        device_service_.async_read_some(impl, buffers, handler);
    }




private:
    device_service_t device_service_;
};

}
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>



#endif // TRIAL_AUDIO_DETAIL_AUDIO_SOCKET_SERVICE_HPP
