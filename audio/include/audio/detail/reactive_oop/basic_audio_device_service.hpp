#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_basic_device_SERVICE_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_basic_device_SERVICE_HPP

#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/detail/addressof.hpp>
#include <boost/asio/detail/bind_handler.hpp>
#include <boost/asio/detail/buffer_sequence_adapter.hpp>
#include <boost/asio/detail/fenced_block.hpp>
#include <boost/asio/detail/noncopyable.hpp>
#include <boost/asio/detail/socket_types.hpp>

#include <poll.h>

#include "audio/detail/config.hpp"
#include "audio/error_codes.hpp"
#include "audio/detail/reactive_oop/reactor.hpp"
#include "audio/detail/reactive_oop/handler/read_op.hpp"
#include "audio/detail/reactive_oop/handler/write_op.hpp"
#include "audio/detail/reactive_oop/handler/null_buffers_op.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {


template <typename Device>
class basic_audio_device_service  {
public:
    // The native type of a descriptor.
    using device_t = Device;
    using handle_t = typename Device::handle_t;

    using descriptor_t = boost::asio::detail::socket_type;


    struct device_data_type : private boost::asio::detail::noncopyable {
    public:

        explicit  device_data_type(device_t&& device) : device_(std::move(device)) {}

        explicit device_data_type(const device_t& device) : device_data_type(std::move(device_t{device})) {}

        device_data_type() : device_data_type(std::move(device_t{})) {}

        device_data_type(device_data_type&& other) : device_(std::move(other.device_)) {

        }

        device_data_type& operator=(device_data_type&& other) {
            auto tmp = device_data_type{std::move(this->device_)};
            this->device_ = std::move(other.device_);
            return *this;
        }

        ~device_data_type() {
            audio::error_code ignored_ec;
            if (device_.is_open())
                device_.close(ignored_ec);

        }

        template<typename ...RuntimeSettings>
        audio::error_code open(audio::error_code& ec, RuntimeSettings... runtime_settings) {
            device_.open(ec, runtime_settings...);
            return ec;
        }

        audio::error_code close(audio::error_code& ec) {
            device_.close(ec);
            return ec;
        }

        template <typename... RuntimeSettings>
        audio::error_code set_options(audio::error_code &ec, RuntimeSettings... runtime_settings) {

            if(device_.set_options(ec, runtime_settings...))
                return ec;

            return ec;
        }

        handle_t native_handle() const {
            return device_.native_handle();
        }

        device_t release() {
            return std::move(device_);
        }

        device_t assign(device_t device) {
            device_ = device;
        }

        device_t device() const {
            return device_;
        }

        device_t device() {
            return device_;
        }

        bool is_open() const {
            return device_.is_open();
        }

        descriptor_t descriptor(audio::error_code& ec) {
            return device_.poll_read(ec);
        }

        device_t device_;

    };

    struct descriptor_data_type : private boost::asio::detail::noncopyable {
    public:

        using state_type = typename Device::state_type;
        using reactor_ptr = reactor*;

        descriptor_data_type() : state_(0), descriptor_(-1), reactor_(nullptr), reactor_data_() {}

        descriptor_data_type(descriptor_data_type&& other) : state_(std::move(other.state_)), descriptor_(std::move(other.descriptor_)), reactor_(std::move(other.reactor_))
        {
            if (reactor_ != nullptr) {
                reactor_->move_descriptor(this->descriptor_, this->reactor_data_, other.reactor_data_);
            } else {
                this->reactor_data_ = std::move(other.reactor_data_);
                other.reactor_data_ = reactor_data_();
            }

            other.descriptor_ = -1;
            other.state_ = 0;
            other.reactor_ = nullptr;
        }


        descriptor_data_type& operator=(descriptor_data_type&& other)
        {
            state_ = std::move(other.state_);
            descriptor_ = std::move(other.descriptor_);
            reactor_ = std::move(other.reactor_);

            if (reactor_ != nullptr) {
                reactor_->move_descriptor(this->descriptor_, this->reactor_data_, other.reactor_data_);
            } else {
                reactor_data_ = std::move(other.reactor_data_);
                other.reactor_data_ = reactor_data_();
            }

            other.descriptor_ = -1;
            other.state_ = 0;
            other.reactor_ = nullptr;
            return *this;
        }

        audio::error_code unsubscribe(audio::error_code& ec) {
            reactor_->deregister_descriptor(descriptor_, reactor_data_, false);
            descriptor_ = -1;
            return ec;
        }

        audio::error_code subscribe(descriptor_t descriptor, audio::error_code& ec) {
            reactor_->register_descriptor(descriptor, reactor_data_);
            descriptor_ = descriptor;
            return ec;
        }

        audio::error_code assign(reactor_ptr reactor_, audio::error_code& ec) {
            this->reactor_ = reactor_;
            return ec;
        }

        audio::error_code release(audio::error_code& ec) {
            reactor_ = nullptr;
            return ec;
        }

        audio::error_code cancel(audio::error_code& ec) {
            reactor_->cancel_ops(descriptor_, reactor_data_);
            return ec;
        }


        // The current state of the descriptor.
        state_type state_;

        // The native descriptor representation.
        descriptor_t descriptor_;

        reactor_ptr reactor_;

        // Per-descri<ptor data used by the reactor.
        reactor::per_descriptor_data reactor_data_;

    };

    // The implementation type of the descriptor.
    class implementation_type : private boost::asio::detail::noncopyable {
    public:

        using reactor_ptr = reactor*;

        implementation_type() : descriptor_data_(), device_data_() {}

        implementation_type(implementation_type &&other) : descriptor_data_(std::move(other.descriptor_data_)), device_data_(std::move(other.device_data_))
        {

        }

        implementation_type& operator=(implementation_type &&other) {
            this->device_data_ = std::move(other.device_data_);
            this->descriptor_data_ = std::move(other.descriptor_data_);
            return *this;
        }

        template <typename... Args>
        audio::error_code open(reactor_ptr reactor_, audio::error_code &ec, Args... args) {

            if(device_data_.open(ec, args...))
                return ec;

            if(descriptor_data_.assign(reactor_, ec))
                return ec;

            if(subscribe(ec))
                return ec;

            return ec;
        }

        template <typename... RuntimeSettings>
        audio::error_code set_options(audio::error_code &ec, RuntimeSettings... runtime_settings) {

            if(device_data_.set_options(ec, runtime_settings...))
                return ec;

            return ec;
        }

        audio::error_code close(audio::error_code &ec) {
            if(unsubscribe(ec))
                return ec;

            if(descriptor_data_.release(ec))
                return ec;

            if(device_data_.close(ec))
                return ec;

            return ec;
        }

        void assign(device_t&& device, reactor_ptr reactor_, audio::error_code& ec) {
            (void) ec;
            device_data_.assign(device);
            descriptor_data_.assign(reactor_, ec);

            if (is_open())
                subscribe();

        }

        device_t release(audio::error_code& ec) {
            if (is_open())
                unsubscribe();

            descriptor_data_.release(ec);
            return device_data_.release();
        }

        audio::error_code cancel(audio::error_code& ec) {
            if(is_open())
                return descriptor_data_.cancel(ec);
        }

        bool is_open() const {
            return device_data_.is_open();
        }

        bool is_non_blocking(audio::error_code& ec) {
            return (descriptor_data_.state_ & device_t::non_blocking) || device_data_.device().set_internal_non_blocking(descriptor_data_.state_, true, ec);
        }

        descriptor_t& descriptor() {
            return descriptor_data_.descriptor_;
        }

        reactor::per_descriptor_data& reactor_data() {
            return descriptor_data_.reactor_data_;
        }

        device_t device() const {
            return device_data_.device();
        }

        handle_t native_handle() const {
            return device_data_.native_handle();
        }

    private:
        audio::error_code subscribe(audio::error_code& ec) {
            descriptor_t descriptor = device_data_.descriptor(ec);
            return descriptor_data_.subscribe(descriptor, ec);
        }

        audio::error_code unsubscribe(audio::error_code& ec) {
            return descriptor_data_.unsubscribe(ec);
        }

        descriptor_data_type descriptor_data_;

        device_data_type device_data_;

    };


public:

    // Constructor.
    basic_audio_device_service(boost::asio::io_service &io_service) : reactor_(boost::asio::use_service<reactor>(io_service))
    {
        reactor_.init_task();
    }

    // Destroy all user-defined handler objects owned by the service.
    void shutdown_service() {}


    // Construct a new descriptor implementation.
    void construct(implementation_type &) {

    }

    // Move-construct a new descriptor implementation.
    void move_construct(implementation_type &impl, implementation_type &other_impl)
    {
        impl = std::move(other_impl);
    }

    // Destroy a descriptor implementation.
    void destroy(implementation_type &impl)
    {
        audio::error_code ignored_ec;

        if(impl.is_open())
            impl.close(ignored_ec);

    }


    /// Assign an existing native device to an audio socket.
    audio::error_code assign(implementation_type &impl, const device_t &device, audio::error_code &ec) {
        destroy(impl);
        impl.assign(device, &reactor_, ec);
        return ec;
    }

    /// Move-assign from another descriptor implementation.
    void move_assign(implementation_type &impl,
                     basic_audio_device_service &,
                     implementation_type &other_impl)
    {
        destroy(impl);
        impl = std::move(other_impl);
    }

    device_t release(implementation_type &impl) {
        audio::error_code ignored_ec;
        return impl.release(ignored_ec);
    }


    /// Get the native device implementation.
    device_t native_device(const implementation_type &impl) const {
        return impl.device();
    }

    /// Get the native device implementation.
    handle_t native_handle(const implementation_type &impl) const {
        return impl.native_handle();
    }

    bool is_open(const implementation_type &impl) const {
        return impl.is_open();
    }

    /// Open an audio socket implementation.
    template <typename... RuntimeSettings>
    audio::error_code open(implementation_type &impl, audio::error_code &ec,
                           RuntimeSettings... runtime_settings) {


        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "open"));

        if(is_open(impl)) {
            ec = audio::error::already_open;
            return ec;
        }

        return impl.open(&reactor_, ec, runtime_settings...);

    }


    /// Close an audio socket implementation.
    audio::error_code close(implementation_type &impl, audio::error_code &ec) {

        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "close"));

        if(is_open(impl)) {
            return impl.close(ec);
        }

        return ec;
    }


    /// Cancel all asynchronous operations associated with the device.
    audio::error_code cancel(implementation_type &impl, audio::error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("descriptor", &impl, "cancel"));
        return impl.cancel(ec);
    }

    template <typename... RuntimeSettings>
    audio::error_code set_options(implementation_type &impl, audio::error_code &ec, RuntimeSettings... runtime_settings) {
        return impl.set_options(ec, runtime_settings...);
    }




    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write(implementation_type &impl,
                      const ConstBufferSequence &buffers, error_code &ec) {
        return write_some(impl, buffers, ec);
    }

    /// Write to an audio socket implementation.
    template <typename ConstBufferSequence>
    std::size_t write_some(implementation_type &impl,
                           const ConstBufferSequence &buffers, error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "sync_write"));

        return impl.device().blocking_write(buffers, ec);
    }




    /// Start an asynchronous write. The data being sent must be valid for the
    /// lifetime of the asynchronous operation.
    template <typename ConstBufferSequence, typename Handler>
    void async_write(implementation_type &impl,
                     const ConstBufferSequence &buffers, Handler &handler) {
        async_write_some(impl, buffers, handler);
    }

    /// Start an asynchronous wait until data can be written without blocking.
    template <typename Handler>
    void async_write(implementation_type &impl,
                     const boost::asio::null_buffers &buffers,
                     Handler &handler) {
        async_write_some(impl, buffers, handler);
    }



    /// Start an asynchronous write. The data being sent must be valid for the
    /// lifetime of the asynchronous operation.
    template <typename ConstBufferSequence, typename Handler>
    void async_write_some(implementation_type &impl,
                          const ConstBufferSequence &buffers,
                          Handler &handler) {
        bool is_continuation =
                boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::write_op<device_t, ConstBufferSequence, Handler> op;
        typename op::ptr p = {
                boost::asio::detail::addressof(handler),
                boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(impl.device(), buffers, handler);

        BOOST_ASIO_HANDLER_CREATION((p.p, "device", &impl, "async_write_some"));

        start_op(impl, reactor::read_op, p.p, is_continuation, true,
                 boost::asio::detail::buffer_sequence_adapter<
                         boost::asio::const_buffer,
                         ConstBufferSequence>::all_empty(buffers));

        p.v = p.p = 0;
    }

    /// Start an asynchronous wait until data can be written without blocking.
    template <typename Handler>
    void async_write_some(implementation_type &impl,
                          const boost::asio::null_buffers &, Handler &handler) {
        bool is_continuation =
                boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::null_buffers_op<device_t, Handler> op;
        typename op::ptr p = {
                boost::asio::detail::addressof(handler),
                boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};

        p.p = new (p.v) op(handler);

        BOOST_ASIO_HANDLER_CREATION(
                (p.p, "device", &impl, "async_write_some(null_buffers)"));

        start_op(impl, reactor::write_op, p.p, is_continuation, false, false);

        p.v = p.p = 0;
    }





    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read(implementation_type &impl,
                     const MutableBufferSequence &buffers, error_code &ec) {
        return read_some(impl, buffers, ec);
    }

    /// Read from an audio socket implementation.
    template <typename MutableBufferSequence>
    std::size_t read_some(implementation_type &impl,
                          const MutableBufferSequence &buffers,
                          error_code &ec) {
        BOOST_ASIO_HANDLER_OPERATION(("device", &impl, "sync_read"));

        return impl.device().blocking_read(buffers, ec);
    }



    // Start an asynchronous read. The buffer for the data being read must be
    // valid for the lifetime of the asynchronous operation.
    template <typename MutableBufferSequence, typename Handler>
    void async_read(implementation_type &impl,
                    const MutableBufferSequence &buffers, Handler &handler) {
        async_read_some(impl, buffers, handler);
    }

    // Wait until data can be read without blocking.
    template <typename Handler>
    void async_read(implementation_type &impl,
                    const boost::asio::null_buffers &buffers,
                    Handler &handler) {
        async_read_some(impl, buffers, handler);
    }

    // Start an asynchronous read. The buffer for the data being read must be
    // valid for the lifetime of the asynchronous operation.
    template <typename MutableBufferSequence, typename Handler>
    void async_read_some(implementation_type &impl,
                         const MutableBufferSequence &buffers,
                         Handler &handler) {
        bool is_continuation =
                boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::read_op<device_t, MutableBufferSequence, Handler> op;
        typename op::ptr p = {
                boost::asio::detail::addressof(handler),
                boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(impl.device(), buffers, handler);

        BOOST_ASIO_HANDLER_CREATION((p.p, "device", &impl, "async_read_some"));

        start_op(impl, reactor::read_op, p.p, is_continuation, true,
                 boost::asio::detail::buffer_sequence_adapter<
                         boost::asio::mutable_buffer,
                         MutableBufferSequence>::all_empty(buffers));

        p.v = p.p = 0;
    }

    // Wait until data can be read without blocking.
    template <typename Handler>
    void async_read_some(implementation_type &impl,
                         const boost::asio::null_buffers &, Handler &handler) {
        bool is_continuation =
                boost_asio_handler_cont_helpers::is_continuation(handler);

        // Allocate and construct an operation to wrap the handler.
        typedef handler::null_buffers_op<device_t, Handler> op;
        typename op::ptr p = {
                boost::asio::detail::addressof(handler),
                boost_asio_handler_alloc_helpers::allocate(sizeof(op), handler), 0};
        p.p = new (p.v) op(handler);

        BOOST_ASIO_HANDLER_CREATION(
                (p.p, "device", &impl, "async_read_some(null_buffers)"));

        start_op(impl, reactor::read_op, p.p, is_continuation, false, false);

        p.v = p.p = 0;
    }




private:
    void start_op(implementation_type &impl, int op_type,
                  boost::asio::detail::reactor_op *op,
                  bool is_continuation, bool is_non_blocking,
                  bool noop)
    {
        if (!noop) {
            if (impl.is_non_blocking(op->ec_)) {
                reactor_.start_op(op_type, impl.descriptor(), impl.reactor_data(), op, is_continuation, is_non_blocking);
                return;
            }
        }

        reactor_.post_immediate_completion(op, is_continuation);
    }

    reactor &reactor_;
};

} // namespace reactive
} // namespace detail
} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>



#endif // TRIAL_AUDIO_DETAIL_REACTIVE_basic_device_SERVICE_HPP