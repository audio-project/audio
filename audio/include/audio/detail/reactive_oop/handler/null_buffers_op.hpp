#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_NULL_BUFFERS_OP_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_NULL_BUFFERS_OP_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <boost/asio/detail/config.hpp>

#include <boost/asio/detail/reactive_null_buffers_op.hpp>


#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {
namespace handler {

    template<typename Device, typename ...Args>
    using null_buffers_op = boost::asio::detail::reactive_null_buffers_op<Args...>;
    
}
}
} // namespace detail
} // namespace audio
} // namespace trial



#endif // TRIAL_AUDIO_DETAIL_REACTIVE_HANDLE_OPS_HANDLER_ALSA_NULL_BUFFERS_OP_HPP