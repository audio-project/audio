#ifndef TRIAL_AUDIO_DETAIL_REACTIVE_device_SERVICE_HPP
#define TRIAL_AUDIO_DETAIL_REACTIVE_device_SERVICE_HPP

#include "audio/detail/reactive_oop/basic_audio_device_service.hpp"
#include "audio/devices/alsa_device.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {
namespace detail {
namespace reactive {

template<typename ...CompileSettings>
using audio_device_service = basic_audio_device_service<devices::alsa_device_t<CompileSettings...>>;

}
}
}
}

#endif