//
// Created by km on 5/28/17.
//

#ifndef AUDIO_DISTRIBUTION_BASIC_ALSA_HPP
#define AUDIO_DISTRIBUTION_BASIC_ALSA_HPP



#include <cerrno>
#include <alsa/asoundlib.h>
#include <type_traits>
#include <audio/options.hpp>
#include <boost/variant.hpp>
#include <boost/asio/detail/consuming_buffers.hpp>
#include <cerrno>
#include <iostream>
#include <audio/util.hpp>
#include <audio/devices/alsa_controller.hpp>
#include <audio/devices/alsa_settings.hpp>

namespace trial {
namespace audio {
namespace devices {


using namespace util;

template<typename Controller, typename Settings>
struct basic_alsa_device_t {
public:
    using state_type   = unsigned char;

#if defined(GENERATING_DOCUMENTATION)
    using settings_t   = alsa_settings_t;
    using controller_t = alsa_controller_t;
    using socket_type  = alsa_controller_t::socket_type;
    using handle_t     = alsa_controller_t::raw_handle_t;
#else
    using settings_t   = Settings;
    using controller_t = Controller;
    using socket_type  = typename controller_t::socket_type;
    using handle_t     = typename controller_t::raw_handle_t;
#endif

    using planar_tag      = options::compile_option_t< options::container_format_t, options::container_format_t::values::planar>;
    using interleaved_tag = options::compile_option_t< options::container_format_t, options::container_format_t::values::interleaved>;

    static const int user_set_non_blocking = 1;
    static const int internal_non_blocking = 2;
    static const int non_blocking = user_set_non_blocking | internal_non_blocking;
    static const int possible_dup = 4;

public:
    template<typename ...RuntimeSettings>
    audio::error_code open(audio::error_code &ec, RuntimeSettings... runtime_settings) {

        std::cout << "-------- Open Device -----------" << std::endl;

        auto device_id = settings_t::template get_setting<options::device_id_t>(runtime_settings...);
        auto direction = settings_t::template get_setting<options::direction_t>(runtime_settings...);

        controller.open(device_id, direction, ec);

        alsa_params params;
        
        snd_pcm_hw_params_alloca(&params.hw);
        
        snd_pcm_sw_params_alloca(&params.sw);

        if (controller.hw_params_any(params.hw, ec))
            return ec;

        if(set_hw_options(ec, params, runtime_settings...))
            return ec;

        options::latency_t buffer_size;
        controller.get_option(params, ec, buffer_size);
        auto buffer_frames = boost::get<units::Frame>(buffer_size.value());

        options::period_t period_size;
        controller.get_option(params, ec, period_size);
        auto period_frames = boost::get<units::Frame>(period_size.value());

        options::min_available_t min_available{period_frames};
        options::start_threshold_t start_threshold((buffer_frames * period_frames) / period_frames);

        if(controller.sw_params_current(params.sw, ec))
            return ec;

        if (!(settings_t::template is_a_compile_time_option<options::min_available_t>::value || is_one_of<options::min_available_t, RuntimeSettings...>::value))
            if(set_sw_options(ec, params, min_available))
                return ec;

        if (!(settings_t::template is_a_compile_time_option<options::start_threshold_t>::value || is_one_of<options::start_threshold_t, RuntimeSettings...>::value))
            if(set_sw_options(ec, params, start_threshold))
                 return ec;

        if(set_sw_options(ec, params, runtime_settings...))

        std::cout << "--------------------------------" << std::endl;
        return ec;
    }

    /*!
     *
     * @return True if the #controller is open
     */
    bool is_open() const {
        return controller.is_open();
    }

    audio::error_code close(audio::error_code &ec) {
        controller.close();
        return ec;
    }

    template<typename ...RuntimeSettings>
    audio::error_code set_options(audio::error_code &ec, RuntimeSettings... runtime_settings) {

        std::cout << "-------- Configure Device -----------" << std::endl;

        static_assert(all_in_list<List<RuntimeSettings...>, sw_settings_list>::value, "Hardware cant be set after");

        if(!is_open()) {
            ec = audio::error::not_connected;
            return ec;
        }

        alsa_params params;
        snd_pcm_hw_params_alloca(&params.hw);

        snd_pcm_sw_params_alloca(&params.sw);

        if(controller.sw_params_current(params.sw, ec))
            return ec;

        if(set_sw_options(ec, params, runtime_settings...))
            return ec;

        std::cout << "--------------------------------" << std::endl;
        return ec;
    }

    /*!
     *
     * @param ec
     * @return True if
     *
     * @pre #is_open must return true
     */
    socket_type poll_read(audio::error_code &ec) {
        return controller.poll_read(ec);
    }

    bool set_internal_non_blocking(state_type& state, bool value, audio::error_code& ec) {

        if (!controller.is_open())
        {
            ec = audio::error::bad_descriptor;
            return false;
        }

        if (!value && (state & user_set_non_blocking))
        {
            // It does not make sense to clear the internal non-blocking flag if the
            // user still wants non-blocking behaviour. Return an error and let the
            // caller figure out whether to update the user-set non-blocking flag.
            ec = audio::error::invalid_argument;
            return false;
        }

        errno = 0;

        // FIX ME
        auto result = 1;

        if (result >= 0)
        {
            ec = audio::error_code();
            if (value)
                state |= internal_non_blocking;
            else
                state &= ~internal_non_blocking;
            return true;
        }

        return false;

    }

    template <typename ConstBufferSequence>
    std::size_t blocking_write(const ConstBufferSequence &buffers, audio::error_code &ec) {
        using pcm_format = typename ConstBufferSequence::pcm_format;

        if(!is_valid_buffer<pcm_format>()) {
            ec = audio::error::channels_invalid;
            return 0;
        }




        return blocking_write_dispatch<pcm_format>(buffers, ec, typename pcm_format::container{});
    };

    template <typename MutableBufferSequence>
    std::size_t blocking_read(MutableBufferSequence &buffers, audio::error_code &ec) {
        using pcm_format = typename MutableBufferSequence::pcm_format;

        if(!is_valid_buffer<pcm_format>()) {
            ec = audio::error::no_protocol_option;
            return 0;
        }


        return blocking_read_dispatch<pcm_format>(buffers, ec, typename pcm_format::container{});
    }

    template <typename ConstBufferSequence>
    bool non_blocking_write(const ConstBufferSequence &buffers, audio::error_code &ec,
                            std::size_t &bytes_transfered) {

        using pcm_format = typename ConstBufferSequence::pcm_format;

        if(!is_valid_buffer<pcm_format>()) {
            ec = audio::error::no_protocol_option;
            return 0;
        }


        return non_blocking_write_dispatch<pcm_format>(buffers, ec, bytes_transfered, typename pcm_format::container{});
    }

    template <typename ConstBufferSequence, typename Buffer>
    bool non_blocking_write(const boost::asio::detail::consuming_buffers<Buffer, ConstBufferSequence> &buffers, audio::error_code &ec,
                            std::size_t &bytes_transfered) {

        using pcm_format = typename ConstBufferSequence::pcm_format;

        if(!is_valid_buffer<pcm_format>()) {
            ec = audio::error::no_protocol_option;
            return 0;
        }


        return non_blocking_write_dispatch<pcm_format>(*buffers.begin(), ec, bytes_transfered, typename pcm_format::container{});
    }

    template <typename MutableBufferSequence>
    bool non_blocking_read(MutableBufferSequence &buffers, audio::error_code &ec,
                           std::size_t &bytes_transfered) {

        using pcm_format = typename MutableBufferSequence::pcm_format;

        if(!is_valid_buffer<pcm_format>()) {
            ec = audio::error::no_protocol_option;
            return 0;
        }

        return non_blocking_read_dispatch<pcm_format>(buffers, ec, bytes_transfered, typename pcm_format::container{});
    }

    handle_t native_handle() const {
        return controller.native_handle();
    }


protected:

    template<typename PCMFormat>
    bool is_valid_buffer() {
        static_assert(settings_t::is_compile_valid(typename PCMFormat::container()), "Invalid Container Format");
        static_assert(settings_t::is_compile_valid(typename PCMFormat::sample()), "Invalid Sample Format");
        static_assert(settings_t::is_compile_valid(typename PCMFormat::channels()), "Invalid Channels Number");

        if(!settings.is_runtime_valid(typename PCMFormat::container()))
            return false;

        if(!settings.is_runtime_valid(typename PCMFormat::sample()))
            return false;

        if(!settings.is_runtime_valid(typename PCMFormat::channels()))
            return false;

        return true;
    }




    /*!
     * Blocking interleaved write
     *
     * @tparam ConstBufferSequence
     * @param buffers
     * @param ec
     * @return Number of bytes written
     */
    template <typename PCMFormat, typename ConstBufferSequence>
    std::size_t blocking_write_dispatch(const ConstBufferSequence &buffers, audio::error_code &ec, interleaved_tag) {

        using pcm_format = PCMFormat;

        controller.prepare(ec);

        // Change PCM handle to blocking mode.
        if(controller.nonblock(0, ec))
            return 0;

        // Prepare the PCM handle.
        if(controller.prepare(ec))
            return 0;

        // Write buffer to ALSA.
        int bytes_written = controller.write_interleaved(
                boost::asio::detail::buffer_cast_helper(buffers),
                boost::asio::detail::buffer_size_helper(buffers) / options::frame_size<pcm_format>::value,
                ec
        ) * options::frame_size<pcm_format>::value;

        if (bytes_written < 0) {
            controller.nonblock(1, ec);
            return 0;
        }

        controller.nonblock(1, ec);
        ec = audio::error_code();
        return bytes_written;
    };

    /*!
     * Blocking planar write
     *
     * @tparam ConstBufferSequence
     * @param buffers
     * @param ec
     * @return Number of bytes written
     */
    template <typename PCMFormat, typename ConstBufferSequence>
    std::size_t blocking_write_dispatch(const ConstBufferSequence &, audio::error_code &, planar_tag) {

        return 0;

    };

    /*!
     * Blocking interleaved read
     *
     * @tparam MutableBufferSequence
     * @param buffers
     * @param ec
     * @return Number of bytes read
     */
    template <typename PCMFormat, typename MutableBufferSequence>
    std::size_t blocking_read_dispatch(MutableBufferSequence &buffers, audio::error_code &ec, interleaved_tag) {

        using pcm_format = PCMFormat;

        controller.prepare(ec);

        // Change PCM handle to blocking mode.
        if(controller.nonblock(0, ec))
            return 0;

        // Prepare the PCM handle.
        if(controller.prepare(ec))
            return 0;

        // Read buffer from ALSA.
        int bytes_read = controller.read_interleaved(
                boost::asio::detail::buffer_cast_helper(buffers),
                boost::asio::detail::buffer_size_helper(buffers) / options::frame_size<pcm_format>::value,
                ec
        ) * options::frame_size<pcm_format>::value;

        if (bytes_read < 0) {
            controller.nonblock(1, ec);
            return 0;
        }

        controller.nonblock(1, ec);
        ec = audio::error_code();
        return bytes_read;
    }

    /*!
     * Blocking Read
     * Planar
     *
     * @tparam MutableBufferSequence
     * @param buffers
     * @param ec
     * @return Number of bytes read
     */
    template <typename PCMFormat, typename MutableBufferSequence>
    std::size_t blocking_read_dispatch(MutableBufferSequence &, audio::error_code &, planar_tag) {

        return 0;
    }




    template <typename PCMFormat, typename ConstBufferSequence>
    bool non_blocking_write_dispatch(const ConstBufferSequence &buffers, audio::error_code &ec, std::size_t &bytes_transferred, interleaved_tag) {

        using pcm_format = PCMFormat;

        // Write buffer to ALSA.
        int bytes_written = controller.write_interleaved(
                boost::asio::detail::buffer_cast_helper(buffers),
                boost::asio::detail::buffer_size_helper(buffers) / options::frame_size<pcm_format>::value,
                ec
        ) * options::frame_size<pcm_format>::value;

        if (bytes_written > 0) {
            ec = audio::error_code();
            bytes_transferred = bytes_written;
        } else {
            bytes_transferred = 0;
            return false;
        }

        return true;
    }

    template <typename PCMFormat, typename ConstBufferSequence>
    bool non_blocking_write_dispatch(const ConstBufferSequence &, audio::error_code &, std::size_t &, planar_tag) {
        return false;
    }



    template <typename PCMFormat, typename MutableBufferSequence>
    bool non_blocking_read_dispatch(MutableBufferSequence &buffers, audio::error_code &ec, std::size_t &bytes_transferred, interleaved_tag) {

        using pcm_format = PCMFormat;

        // Read buffer from ALSA.
        int bytes_read = controller.read_interleaved(
                boost::asio::detail::buffer_cast_helper(buffers),
                boost::asio::detail::buffer_size_helper(buffers) / options::frame_size<pcm_format>::value,
                ec
        ) * options::frame_size<pcm_format>::value;

        if (bytes_read > 0) {
            ec = audio::error_code();
            bytes_transferred = bytes_read;
        } else {
            bytes_transferred = 0;
            return false;
        }

        return true;

    }

    template <typename PCMFormat, typename MutableBufferSequence>
    bool non_blocking_read_dispatch(MutableBufferSequence &, audio::error_code &, std::size_t &, planar_tag) {
        return false;
    }


    template<typename ...RuntimeSettings>
    audio::error_code set_hw_options(audio::error_code &ec,
                                     alsa_params params,
                                     RuntimeSettings... runtime_settings) {

        if(settings.template set_option<options::resample_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::container_format_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::sample_format_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::channels_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::sample_rate_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::latency_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::period_t >(controller, params, ec, runtime_settings...))
            return ec;



        if(controller.hw_params(params.hw, ec))
            return ec;

        return ec;
    }

    template<typename ...RuntimeSettings>
    audio::error_code set_sw_options(audio::error_code &ec,
                                     alsa_params params,
                                     RuntimeSettings... runtime_settings) {

        if(settings.template set_option<options::start_threshold_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(settings.template set_option<options::min_available_t >(controller, params, ec, runtime_settings...))
            return ec;

        if(controller.sw_params(params.sw, ec))
            return ec;

        return ec;
    }

    controller_t controller; //!< Controller - Wrapper for ALSA
    settings_t settings; //!< Controls the settings - needs controller to act

};


}
}
}



#endif //AUDIO_DISTRIBUTION_BASIC_ALSA_HPP
