//
// Created by km on 5/28/17.
//

#ifndef AUDIO_DISTRIBUTION_BASIC_ALSA_SETTINGS_HPP
#define AUDIO_DISTRIBUTION_BASIC_ALSA_SETTINGS_HPP


#include <cerrno>
#include <type_traits>
#include <audio/options.hpp>
#include <boost/variant.hpp>
#include <iostream>
#include <audio/util.hpp>
#include <audio/devices/alsa_controller.hpp>


namespace trial {
namespace audio {
namespace devices {

using namespace util;


//region Setting Types

template<typename Option>
struct RuntimeSetting {

    audio::error_code set_option(alsa_controller_t& , alsa_params , audio::error_code &ec) {
        return ec;
    }

    template<typename ...Settings>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, Option value, Settings...) {
        return alsa_controller.set_option(params, ec, value);
    }

    template<typename T, typename ...Settings>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, T , Settings... settings) {
        return set_option(alsa_controller, params, ec, settings...);
    }

    Option get_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec) {
        Option value;
        ec = alsa_controller.get_option(alsa_controller, params, ec, value);
        return value;
    }


};

template<typename Option>
struct CachedSetting {

    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec) {
        (void)alsa_controller;
        (void)params;
        return ec;
    }

    template<typename ...Settings>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, Option value, Settings...) {
        value_ = value;
        return alsa_controller.set_option(params, ec, value);
    }

    template<typename T, typename ...Settings>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, T , Settings... settings) {
        return set_option(alsa_controller, params, ec, settings...);
    }

    Option get_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec) const {
        (void) alsa_controller;
        (void) params;
        (void) ec;
        return value_;
    }

    Option get_option() const  {
        return value_;
    }

private:
    Option value_;

};

template<typename CompileOption, typename Option>
struct CompiletimeSetting {

    template<typename ...Settings>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, Settings...) {
        static_assert(!is_one_of<Option, Settings...>::value, "Cant set compile time");
        return alsa_controller.set_option(params, ec, CompileOption::value());
    }

    Option get_option(alsa_controller_t& , alsa_params , audio::error_code &) const {
        return CompileOption::value();
    }

    Option get_option() const {
        return CompileOption::value();
    }

};

//endregion

namespace generators {

//region Generate the Setting Type for the Option

// Runtime settings need to communicate with the driver to be read
template<typename T, typename U, bool Compiled, bool Cached>
struct setting_generator_impl_t {
    using type = RuntimeSetting<T>;
};

// Cached keeps a copy
template<typename T, typename U>
struct setting_generator_impl_t<T, U, false, true> {
    using type = CachedSetting<T>;
};

// Compile time cannot be set
template<typename Option, typename ...CompiledSettings, bool Cached>
struct setting_generator_impl_t<Option, List<CompiledSettings...>, true, Cached> {
    using compiled_setting = typename get_compiled_setting<Option, CompiledSettings...>::type;
    using type = CompiletimeSetting<compiled_setting, Option>;
};

// Find what kind of class is needed for the option
template<typename T, typename CachedSettingsList, typename CompileSettingsList>
struct setting_generator_t;

template<typename T, typename ...CachedSettings, typename ...CompileSettings>
struct setting_generator_t<T, List<CachedSettings...>, List<CompileSettings...>> {

using cached   = is_in_list<T, List<CachedSettings...>>;
using compiled = is_in_list<T, List<typename CompileSettings::option_type...>>;

using type = typename setting_generator_impl_t<T, List<CompileSettings...>, compiled::value, cached::value>::type;
};


//endregion

}





//region Settings



template<typename ...CompileSettings>
struct basic_alsa_settings_t;


template<typename ...Settings, typename ...CachedSettings, typename ...CompileSettings>
struct basic_alsa_settings_t<List<Settings...>, List<CachedSettings...>, List<CompileSettings...>> : generators::setting_generator_t<Settings, List<CachedSettings...>, List<CompileSettings...>>::type... {

    basic_alsa_settings_t() {
        static_assert(!has_duplicate<Settings...>::value, "Settings List has duplicates");
        static_assert(!has_duplicate<CompileSettings...>::value, "Compile Settings has duplicates");
    }

    template<typename Option>
    using base = typename generators::setting_generator_t<Option, List<CachedSettings...>, List<CompileSettings...>>::type;

    template<typename Option, typename ...Args>
    audio::error_code set_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec, Args... args) {
        static_assert(!has_duplicate<Args...>::value, "Runtime Settings has duplicates");
        return base<Option>::set_option(alsa_controller, params, ec, args...);
    }

    template<typename Option>
    Option get_option(alsa_controller_t& alsa_controller, alsa_params params, audio::error_code &ec) {
        return base<Option>::get_option(alsa_controller, params, ec);
    }

    template<typename Option>
    Option get_cached_option() const {
        return base<Option>::get_option();
    }

    template<typename CompileOption>
    static constexpr bool is_compile_valid(CompileOption) {
        /// Should only fail if CompileTimeOption IS a Compiled setting AND has the wrong value.
        /// Therefor always true if it is not a setting.
        return !is_a_compile_time_option<CompileOption>::value || has_correct_compile_time_value<CompileOption>::value;
    }

    template<typename RuntimeOption>
    bool is_runtime_valid(RuntimeOption) const {
        static_assert(is_a_compile_time_option<RuntimeOption>::value || is_a_cached_option<RuntimeOption>::value, "Need to be compile or cached");

        return is_a_compile_time_option<RuntimeOption>::value || is_valid_cached_setting<RuntimeOption>();

    }

    template<typename CompileOption>
    bool is_valid_cached_setting() const {
        return CompileOption::value().value() == get_cached_option<typename CompileOption::option_type>().value();
    };


    template<typename Option, typename ...RuntimeSettings>
    static Option get_setting(RuntimeSettings... runtime_settings) {
        return get_runtime_setting<Option>( CompileSettings::value()..., runtime_settings...);
    };

    template<typename Option, typename U = void>
    struct is_a_cached_option {
        static const bool value = is_one_of<Option, CachedSettings...>::value;
    };

    template<typename CompileTimeOption>
    struct is_a_cached_option<CompileTimeOption, typename always_void<typename CompileTimeOption::option_type>::type> {
        static const bool value = is_one_of<typename CompileTimeOption::option_type, CachedSettings...>::value;
    };


    template<typename Option, typename U = void>
    struct is_a_compile_time_option {
        static const bool value = is_one_of<Option, typename CompileSettings::option_type...>::value;
    };

    template<typename CompileTimeOption>
    struct is_a_compile_time_option<CompileTimeOption, typename always_void<typename CompileTimeOption::option_type>::type> {
        static const bool value = is_one_of<typename CompileTimeOption::option_type, typename CompileSettings::option_type...>::value;
    };

    template<typename CompileTimeOption>
    struct has_correct_compile_time_value {
        static const bool value = is_one_of<CompileTimeOption, CompileSettings...>::value;
    };

};

//endregion


}
}
}

#endif //AUDIO_DISTRIBUTION_BASIC_ALSA_SETTINGS_HPP
