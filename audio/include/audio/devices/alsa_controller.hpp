//
// Created by km on 5/27/17.
//

#ifndef AUDIO_DISTRIBUTION_ALSA_BASE_HPP
#define AUDIO_DISTRIBUTION_ALSA_BASE_HPP

#include <boost/asio/detail/socket_types.hpp>
#include <alsa/asoundlib.h>
#include <audio/error_codes.hpp>
#include <audio/options.hpp>
#include <audio/util.hpp>
#include <boost/variant.hpp>

namespace trial {
namespace audio {
namespace devices {

using namespace util;

struct alsa_params {
    snd_pcm_sw_params_t *sw;
    snd_pcm_hw_params_t *hw;
};

struct alsa_controller_t {

    using socket_type = boost::asio::detail::socket_type;
    using raw_handle_t = snd_pcm_t*;

    struct hw_configure {
        explicit hw_configure(const raw_handle_t &handle, snd_pcm_hw_params_t* hw_params, audio::error_code &ec) : handle(handle), hw_params(hw_params), ec(ec) {}

        const raw_handle_t &handle;
        snd_pcm_hw_params_t* hw_params;
        audio::error_code &ec;
    };

    audio::error_code open(options::device_id_t device_id, options::direction_t direction, audio::error_code &ec) {

        const char* device_id_ = device_id.value().c_str();
        snd_pcm_stream_t direction_ = (direction.value() == options::direction_t::values::playback ? SND_PCM_STREAM_PLAYBACK : SND_PCM_STREAM_CAPTURE);



        error_wrapper(
                snd_pcm_open(
                        &handle,
                        device_id_,
                        direction_,
                        SND_PCM_NONBLOCK
                ),
                ec
        );

        return ec;
    }

    bool is_open() const {
        return handle != nullptr;
    }

    void close() {
        snd_pcm_close(handle);
        handle = nullptr;
    }

    socket_type poll_read(audio::error_code& ec) {

        int count = snd_pcm_poll_descriptors_count(handle);
        if (count == 0) {
            return -1;
        }

        struct pollfd ufds[count];

        int result = error_wrapper(snd_pcm_poll_descriptors(handle, ufds, count), ec);
        if (result < 0) {
            return -1;
        }

        // DO READ
        return ufds[0].fd;
    }


    audio::error_code hw_params_any(snd_pcm_hw_params_t *hw_params, audio::error_code &ec) {
        int err = snd_pcm_hw_params_any(handle, hw_params);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code sw_params_current(snd_pcm_sw_params_t *sw_params, audio::error_code &ec) {
        int err = snd_pcm_sw_params_current(handle, sw_params);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code hw_params_current(snd_pcm_hw_params_t *hw_params, audio::error_code &ec) {
        int err = snd_pcm_hw_params_current(handle, hw_params);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code hw_params(snd_pcm_hw_params_t *hw_params, audio::error_code &ec) {
        int err = snd_pcm_hw_params(handle, hw_params);

        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code sw_params(snd_pcm_sw_params_t *sw_params, audio::error_code &ec) {
        int err = snd_pcm_sw_params(handle, sw_params);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code prepare(audio::error_code &ec) {
        int err = snd_pcm_prepare(handle);
        if (err < 0) {
            ec = audio::error::already_open;
            return ec;
        }
        return ec;
    }

    audio::error_code nonblock(int i, audio::error_code &ec) {
        int err = snd_pcm_nonblock(handle, i);
        if (err < 0) {
            ec = audio::error::already_open;
            return ec;
        }
        return ec;
    }





    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::container_format_t &setting) {



        using enum_t = options::container_format_t::values;

        snd_pcm_access_t value;

        std::cout << "Container Format: ";

        switch(setting.value()) {
            case enum_t::interleaved: value = SND_PCM_ACCESS_RW_INTERLEAVED;
                std::cout << "SND_PCM_ACCESS_RW_INTERLEAVED" << std::endl;
                break;
            case enum_t::planar:      value = SND_PCM_ACCESS_RW_NONINTERLEAVED ;
                std::cout << "SND_PCM_ACCESS_RW_NONINTERLEAVED" << std::endl;
                break;
            default:                  value = SND_PCM_ACCESS_RW_INTERLEAVED;
                std::cout << "SND_PCM_ACCESS_RW_INTERLEAVED - DEFAULT" << std::endl;
                break;
        }

        int err = snd_pcm_hw_params_set_access(handle, params.hw, value);
        if(err < 0)
            ec = audio::error::no_protocol_option;
        else
            ec = audio::error_code();
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::sample_format_t &setting) {

        std::cout << "Sample Format: ";
        using enum_t = options::sample_format_t::values;

        snd_pcm_format_t value;

        switch(setting.value()) {
            case enum_t::unsigned16_le: value = SND_PCM_FORMAT_U16_LE;
                std::cout << "SND_PCM_FORMAT_U16_LE" << std::endl;
                break;
            case enum_t::signed16_le:   value = SND_PCM_FORMAT_S16_LE ;
                std::cout << "SND_PCM_FORMAT_S16_LE" << std::endl;
                break;
            case enum_t::signed16_be:   value = SND_PCM_FORMAT_S16_BE;
                std::cout << "SND_PCM_FORMAT_S16_BE" << std::endl;
                break;
            case enum_t::signed32_le:   value = SND_PCM_FORMAT_S32_LE ;
                std::cout << "SND_PCM_FORMAT_S32_LE" << std::endl;
                break;
            case enum_t::signed32_be:   value = SND_PCM_FORMAT_S32_BE;
                std::cout << "SND_PCM_FORMAT_S32_BE" << std::endl;
                break;
            default:                    value = SND_PCM_FORMAT_UNKNOWN;
                std::cout << "SND_PCM_FORMAT_UNKNOWN" << std::endl;
                break;
        }

        int err = snd_pcm_hw_params_set_format(handle, params.hw, value);
        if(err < 0)
            ec = audio::error::no_protocol_option;
        else
            ec = audio::error_code();
        return ec;
    }

    struct latency_configure : public boost::static_visitor<>, hw_configure
    {

        using hw_configure::hw_configure;

        void operator()(units::Microsecond microseconds) const {
            std::cout << "Latency size: " << microseconds << std::endl;

            unsigned int value = static_cast<unsigned long long int>(microseconds);
            int dir;

            int err = snd_pcm_hw_params_set_buffer_time_near(handle, hw_params, &value, &dir);
            if(err < 0)
                ec = audio::error::already_open;
        }
        void operator()(units::Frame frames) const {
            std::cout << "Latency size: " << frames << std::endl;

            snd_pcm_uframes_t value = static_cast<unsigned long long int>(frames);

            int err = snd_pcm_hw_params_set_buffer_size_near(handle, hw_params, &value);
            if(err < 0)
                ec = audio::error::already_open;
        }
    };

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::latency_t &setting) {
        auto data = latency_configure{handle, params.hw, ec};
        boost::apply_visitor(data, setting.value());
        return ec;
    }

    struct period_configure : public boost::static_visitor<>, hw_configure
    {

        using hw_configure::hw_configure;

        void operator()(units::Microsecond microseconds) const {
            std::cout << "Period size: " << microseconds << std::endl;

            unsigned int value = static_cast<unsigned long long int>(microseconds);
            int dir;

            int err = snd_pcm_hw_params_set_period_time_near(handle, hw_params, &value, &dir);
            if (err < 0) {
                ec = audio::error::already_open;
                return;
            }
        }
        void operator()(units::Frame frames) const {
            std::cout << "Period size: " << frames << std::endl;

            snd_pcm_uframes_t value = static_cast<unsigned long long int>(frames);
            int dir;

            int err = snd_pcm_hw_params_set_period_size_near(handle, hw_params, &value, &dir);
            if (err < 0) {
                ec = audio::error::already_open;
                return;
            }
        }
    };

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::period_t &setting) {
        boost::apply_visitor(period_configure{handle, params.hw, ec}, setting.value());
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::sample_rate_t &setting) {
        std::cout << "Sample rate: " << setting.value() << std::endl;

        unsigned int value = static_cast<unsigned long long int>(setting.value());

        int err = snd_pcm_hw_params_set_rate_near(handle, params.hw, &value, 0);
        if(err < 0)
            ec = audio::error::already_open;
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::channels_t &setting) {
        std::cout << "Channels: " << setting.value() << std::endl;

        unsigned int value = setting.value();

        int err = snd_pcm_hw_params_set_channels(handle, params.hw, value);
        if(err < 0)
            ec = audio::error::already_open;
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::resample_t &setting) {
        std::cout << "Alsa may resample: " << (setting.value() ? "True" : "False") << std::endl;

        int value = (setting.value() ? 1 : 0);

        int err = snd_pcm_hw_params_set_rate_resample(handle, params.hw, value);
        if(err < 0)
            ec = audio::error::already_open;
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::start_threshold_t &setting) {
        std::cout << "Start Threshold: " << setting.value() << std::endl;

        int err = snd_pcm_sw_params_set_start_threshold(handle, params.sw, setting.value().value);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;
    }

    audio::error_code set_option(alsa_params params, audio::error_code &ec, const options::min_available_t &setting) {
        std::cout << "Min Available: " << setting.value() << std::endl;

        int err = snd_pcm_sw_params_set_avail_min(handle, params.sw, setting.value().value);
        if (err < 0) {
            ec = audio::error::no_protocol_option;
            return ec;
        }
        return ec;

    }



    audio::error_code get_option(alsa_params params, audio::error_code &ec, options::latency_t& setting) {
        long unsigned int buffer_size;
        snd_pcm_hw_params_get_buffer_size(params.hw, &buffer_size);
        setting = options::latency_t{units::Frame{buffer_size}};
        std::cout << "Read: Buffer Size: " << buffer_size << " frames" << std::endl;
        return ec;
    }

    audio::error_code get_option(alsa_params params, audio::error_code &ec, options::period_t& setting) {
        long unsigned int period_size;
        snd_pcm_hw_params_get_period_size(params.hw, &period_size, NULL);
        setting = options::period_t{units::Frame{period_size}};
        std::cout << "Read: Period Size: " << period_size << " frames" << std::endl;
        return ec;
    }

    int write_interleaved(const void* data, std::size_t length_frames, audio::error_code& ec) {
        return error_wrapper(snd_pcm_writei(handle, data, length_frames ), ec);
    }

    int read_interleaved(void* data, std::size_t length_frames, audio::error_code& ec) {
        return error_wrapper(snd_pcm_readi(handle, data, length_frames ), ec);
    }

    raw_handle_t native_handle() const {
        return handle;
    }


private:
    raw_handle_t handle;
};


}
}
}



#endif //AUDIO_DISTRIBUTION_ALSA_BASE_HPP
