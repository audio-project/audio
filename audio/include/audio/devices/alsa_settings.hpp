//
// Created by km on 5/28/17.
//

#ifndef AUDIO_DISTRIBUTION_ALSA_SETTINGS_HPP
#define AUDIO_DISTRIBUTION_ALSA_SETTINGS_HPP

#include <audio/devices/basic_alsa_settings.hpp>
#include <audio/util.hpp>

namespace trial {
namespace audio {
namespace devices {

using namespace util;

using settings_list = List<
        options::sample_format_t,
        options::container_format_t,
        options::latency_t,
        options::min_available_t,
        options::period_t,
        options::start_threshold_t,
        options::channels_t,
        options::direction_t,
        options::device_id_t,
        options::sample_rate_t,
        options::resample_t
>;

using sw_settings_list = List<
        options::min_available_t,
        options::start_threshold_t
>;

using cached_list = List<
        options::sample_format_t,
        options::container_format_t,
        options::channels_t
>;

template<typename CompileSettings>
using alsa_settings_t = basic_alsa_settings_t<settings_list, cached_list, CompileSettings>;

}
}
}


#endif //AUDIO_DISTRIBUTION_ALSA_SETTINGS_HPP
