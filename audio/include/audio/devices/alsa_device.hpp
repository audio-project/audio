//
// Created by km on 5/1/17.
//

#ifndef TRIAL_AUDIO_DEVICES_alsa_t_HPP
#define TRIAL_AUDIO_DEVICES_alsa_t_HPP

#include <audio/devices/basic_alsa_device.hpp>
#include <audio/util.hpp>

namespace trial {
namespace audio {
namespace devices {

using namespace util;

template<typename ...CompileSettings>
using alsa_device_t = basic_alsa_device_t<alsa_controller_t, alsa_settings_t<List<CompileSettings...>>>;

}
}
}


#endif //TRIAL_AUDIO_DEVICES_alsa_t_HPP
