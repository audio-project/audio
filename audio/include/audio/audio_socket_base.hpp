#ifndef TRIAL_AUDIO_AUDIO_SOCKET_BASE_HPP
#define TRIAL_AUDIO_AUDIO_SOCKET_BASE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <string>
#include <boost/asio/detail/config.hpp>

#include <audio/error_codes.hpp>
#include <audio/options.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {


class audio_socket_base {
public:

protected:
    ~audio_socket_base() {
    }
};

} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#include <audio/impl/audio_socket_base.hpp>

#if defined(AUDIO_HEADER_ONLY)
# include <audio/impl/audio_socket_base.ipp>
#endif // defined(AUDIO_HEADER_ONLY)

#endif // TRIAL_AUDIO_AUDIO_SOCKET_BASE_HPP