#ifndef TRIAL_AUDIO_AUDIO_SOCKET_HPP
#define TRIAL_AUDIO_AUDIO_SOCKET_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/config.hpp>

#include "audio/basic_audio_socket.hpp"
#include "audio/audio_socket_service.hpp"
#include "audio/detail/audio_device_service.hpp"

namespace trial {
namespace audio {
    

template<typename ...CompileTimeOptions>
using audio_socket = basic_audio_socket<
        audio_socket_service_t<
                CompileTimeOptions...
        >
>;

} // namespace audio
} // namespace trial

#endif // TRIAL_AUDIO_AUDIO_SOCKET_HPP
