#ifndef TRIAL_AUDIO_BASIC_AUDIO_SOCKET_HPP
#define TRIAL_AUDIO_BASIC_AUDIO_SOCKET_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/config.hpp>

#include <string>
#include <boost/asio/basic_io_object.hpp>
#include <boost/asio/detail/handler_type_requirements.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/detail/throw_error.hpp>

#include "audio/error_codes.hpp"

#include "audio_socket_base.hpp"
#include "audio_socket_service.hpp"

#include <boost/asio/detail/push_options.hpp>

namespace trial {
namespace audio {

template <typename AudioSocketService>
class basic_audio_socket
	: public boost::asio::basic_io_object<AudioSocketService>,
    public audio_socket_base
{
public:
    typedef typename AudioSocketService::device_t device_t;

    typedef typename AudioSocketService::handle_t handle_t;

	/// Construct a basic_audio_socket without opening it.
    explicit basic_audio_socket(boost::asio::io_service& io_service)
        : boost::asio::basic_io_object<AudioSocketService>(io_service)
    {
    }

    /// Construct and open a basic_audio_socket.
    template<typename ...Settings>
    explicit basic_audio_socket(boost::asio::io_service& io_service, 
        Settings... settings)
            : boost::asio::basic_io_object<AudioSocketService>(io_service)
    {
        audio::error_code ec;
        open(ec, settings...);
        boost::asio::detail::throw_error(ec, "open");
    }

    /// Construct and open a basic_audio_socket.
    explicit basic_audio_socket(boost::asio::io_service& io_service,
                              const device_t& device)
        : boost::asio::basic_io_object<AudioSocketService>(io_service)
    {
        audio::error_code ec;
        this->get_service().assign(this->get_implementation(), device, ec);
        boost::asio::detail::throw_error(ec, "assign");
    }

    /// Open
    template<typename ...Settings>
    audio::error_code open(audio::error_code& ec, Settings... settings)
    {
        this->get_service().open(this->get_implementation(), ec, settings...);
        boost::asio::detail::throw_error(ec, "open");
        return ec;
    }

    /// Close

    audio::error_code close(audio::error_code& ec)
    {
        return this->get_service().close(this->get_implementation(), ec);
    }

    void close()
    {
        audio::error_code ec;
        this->get_service().close(this->get_implementation(), ec);
        boost::asio::detail::throw_error(ec, "close");
    }

    template <typename... RuntimeSettings>
    audio::error_code set_options(audio::error_code &ec, RuntimeSettings... runtime_settings) {
        return this->get_service().set_options(this->get_implementation(), ec, runtime_settings...);
    }


    /// Read
    template <typename MutableBufferSequence>
    std::size_t read(const MutableBufferSequence& buffers)
    {
        audio::error_code ec;
        std::size_t s = this->get_service().read(
            this->get_implementation(), buffers, ec);
        boost::asio::detail::throw_error(ec, "read");
        return s;
    }

    template <typename MutableBufferSequence>
    std::size_t read(const MutableBufferSequence& buffers, audio::error_code& ec)
    {
        return this->get_service().read(
            this->get_implementation(), buffers, ec);
    }

 	/// Write
	template <typename ConstBufferSequence>
    std::size_t write(const ConstBufferSequence& buffers)
    {
        audio::error_code ec;
        std::size_t s = this->get_service().write(
            this->get_implementation(), buffers, ec);
        boost::asio::detail::throw_error(ec, "write");
        return s;
    }

    template <typename ConstBufferSequence>
    std::size_t write(const ConstBufferSequence& buffers, audio::error_code& ec)
    {
        return this->get_service().write(this->get_implementation(), buffers, ec);
    }

    /// Write
    template <typename ConstBufferSequence>
    std::size_t write_some(const ConstBufferSequence& buffers)
    {
        audio::error_code ec;
        std::size_t s = this->get_service().write_some(
                this->get_implementation(), buffers, ec);
        boost::asio::detail::throw_error(ec, "write_some");
        return s;
    }

    template <typename ConstBufferSequence>
    std::size_t write_some(const ConstBufferSequence& buffers, audio::error_code& ec)
    {
        return this->get_service().write_some(this->get_implementation(), buffers, ec);
    }

    template <typename ConstBufferSequence, typename WriteHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(WriteHandler, void (audio::error_code, std::size_t))
    async_write(const ConstBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(WriteHandler) handler)
    {
    
        //Scoped due to namespaces
        {
            // If you get an error on the following line it means that your handler does
            // not meet the documented type requirements for a ReadHandler.
            using namespace boost::asio;
            BOOST_ASIO_WRITE_HANDLER_CHECK(WriteHandler, handler) type_check;
        }

        return this->get_service().async_write(this->get_implementation(), buffers, BOOST_ASIO_MOVE_CAST(WriteHandler)(handler));
    }
    
    template <typename ConstBufferSequence, typename WriteHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(WriteHandler, void (audio::error_code, std::size_t))
    async_write_some(const ConstBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(WriteHandler) handler)
    {
    
        //Scoped due to namespaces
        {
            // If you get an error on the following line it means that your handler does
            // not meet the documented type requirements for a ReadHandler.
            using namespace boost::asio;
            BOOST_ASIO_WRITE_HANDLER_CHECK(WriteHandler, handler) type_check;
        }

        return this->get_service().async_write_some(this->get_implementation(), buffers, BOOST_ASIO_MOVE_CAST(WriteHandler)(handler));
    }

    template <typename MutableBufferSequence, typename ReadHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(ReadHandler, void (audio::error_code, std::size_t))
    async_read(const MutableBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(ReadHandler) handler)
    {
        
        //Scoped due to namespaces
        {
            // If you get an error on the following line it means that your handler does
            // not meet the documented type requirements for a ReadHandler.
            using namespace boost::asio;
            BOOST_ASIO_READ_HANDLER_CHECK(ReadHandler, handler) type_check;
        }
    
        return this->get_service().async_read(this->get_implementation(),
            buffers, BOOST_ASIO_MOVE_CAST(ReadHandler)(handler));
    }
    
    template <typename MutableBufferSequence, typename ReadHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(ReadHandler, void (audio::error_code, std::size_t))
    async_read_some(const MutableBufferSequence& buffers, BOOST_ASIO_MOVE_ARG(ReadHandler) handler)
    {
        
        //Scoped due to namespaces
        {
            // If you get an error on the following line it means that your handler does
            // not meet the documented type requirements for a ReadHandler.
            using namespace boost::asio;
            BOOST_ASIO_READ_HANDLER_CHECK(ReadHandler, handler) type_check;
        }
    
        return this->get_service().async_read_some(this->get_implementation(),
            buffers, BOOST_ASIO_MOVE_CAST(ReadHandler)(handler));
    }

    handle_t native_handle() const
    {
        return this->get_service().native_handle(this->get_implementation());
    }

    device_t native_device() const
    {
        return this->get_service().native_device(this->get_implementation());
    }
};

} // namespace audio
} // namespace trial

#include <boost/asio/detail/pop_options.hpp>

#endif // TRIAL_BASIC_AUDIO_SOCKET_HPP