#ifndef TRIAL_AUDIO_HPP
#define TRIAL_AUDIO_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "audio/audio_socket.hpp"
#include "audio/audio_socket_base.hpp"
#include "audio/audio_socket_service.hpp"
#include "audio/basic_audio_socket.hpp"

#endif // TRIAL_AUDIO_HPP
