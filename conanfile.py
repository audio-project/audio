from conans import ConanFile
from conans import CMake


class ConanGTestExample(ConanFile):
    name = "audio"
    version = "0.0.1"
    settings = "os", "arch", "compiler", "build_type"
    requires = "Boost/1.60.0@lasote/stable"
    generators = "cmake", "txt"
    options = {
        "shared": [True, False],
        "impl": ["alsa", "demo", "directsound"],
        "reactor" : ["epoll", "select"]
    }
    exports = "CMakeLists.txt"
    exports_sources = "audio*"
    default_options = "shared=False", "impl=alsa", "reactor=select", "gtest:shared=False", "Boost:header_only=True"
    
    def requirements(self):
        if self.scope.build_tests:
            self.requires("gtest/aa148e@KalleDK/stable", dev=True)

    def build(self):
        print(self._scope)
        build_defs = {"BUILD_SHARED_LIBS": self.options.shared}
        build_defs["DISABLE_EPOLL"] = "ON" if (self.options.reactor == "select") else "OFF"
        build_defs["WITH_ALSA"] = (self.options.impl == "alsa")
        if self.scope.build_tests:
            build_defs["BUILD_TESTS"] = "1"
        if self.scope.build_samples:
            build_defs["BUILD_SAMPLES"] = "1"
        cmake = CMake(self)
        cmake.configure(source_dir=self.conanfile_directory, build_dir="./", defs=build_defs)
        cmake.build()
        if self.scope.build_tests:
            self.run("ctest")

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def package(self):
        self.copy("*.hpp", "include", "audio/include/audio")
        self.copy(pattern="*.a", dst="lib", src=".", keep_path=False)
        self.copy(pattern="*.lib", dst="lib", src=".", keep_path=False)
        self.copy(pattern="*.dll", dst="bin", src=".", keep_path=False)
        self.copy(pattern="*.so*", dst="lib", src=".", keep_path=False)
        self.copy(pattern="*.dylib*", dst="lib", src=".", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ['audio']
